import 'package:Unihub/util/constants.dart';
import 'package:http/http.dart' as http;
import 'package:Unihub/model/department.dart';

class DepartmentListService {
  static var client = http.Client();

  static Future<Department> fetchDepartmentList(String token, String universityId) async {
    var response = await client.get(
      Uri.parse(apiUrl + "vendor/department?university=$universityId"),
        headers: {
          'Authorization': 'Bearer ' + token
        }
    );

    var jsonString = response.body;
    print(jsonString);
    return departmentFromJson(jsonString);
  }
}
