import 'dart:convert';

import 'package:Unihub/model/student.dart';
import 'package:Unihub/util/constants.dart';
import 'package:http/http.dart' as http;
import 'package:Unihub/model/signup_info.dart';

class StudentSignupService {
  static var client = http.Client();

  static Future<SignupInfo> fetchSignupInfo(Student student) async {
    var url = Uri.parse(apiUrl + "user/student");
    Map data = {
      "student_name": "Amali Vidurangi",
      "user_name":"amal",
      "dob": "12-01-2003",
      "email":"email@gmail.com",
      "country":"Sri Lanka",
      "city":"Colombo",
      "contact_no":["90087677876"],
      "language":"English",
      "university":"London Met",
      "sec_education_level":"A/L",
      "school":"Metro Main",
      "pri_education_level":"O/L",
      "interested_area":"Technology",
      "interested_subject":"IT",
      "status": 1
    }
    ;

    var body = json.encode(data);

    var response = await http.post(url,
        headers: {"Content-Type": "application/json"}, body: body);

    if (response.statusCode == 200) {
      print('Success');
    } else {
      print('error ' + response.statusCode.toString());
    }

    var jsonString = response.body;
    print(jsonString);
    return signupInfoFromJson(jsonString);
  }
}
