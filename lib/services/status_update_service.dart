import 'package:Unihub/util/constants.dart';
import 'package:get/get_connect/connect.dart';

class StatusUpdateService extends GetConnect {

  Future<Response> updateUniversityStatus(String id, String status) async =>
      await post(apiUrl + "vendor/status/university", {'id': id, 'status': status});

  Future<Response> updateCampusStatus(String id, String status) async =>
      await post(apiUrl + "vendor/status/campus", {'id': id, 'status': status});
}
