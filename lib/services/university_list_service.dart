import 'package:Unihub/model/university.dart';
import 'package:Unihub/util/constants.dart';
import 'package:http/http.dart' as http;

class UniversityListService {
  static var client = http.Client();

  static Future<University> fetchUniversityList(String token) async {
    var response = await client.get(
      Uri.parse(apiUrl + "vendor/university"),
        headers: {
          'Authorization': 'Bearer ' + token
        }
    );

    var jsonString = response.body;
    print(jsonString);
    return universityFromJson(jsonString);
  }
}