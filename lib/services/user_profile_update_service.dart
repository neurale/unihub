import 'package:Unihub/model/user_profile.dart';
import 'package:Unihub/util/constants.dart';
import 'package:get/get_connect/connect.dart';

class UserProfileUpdateService extends GetConnect {
  Future<Response> updateUserProfile(Profile profile, String token) => post(apiUrl + "auth/profile", {
        {
          "first_name": profile.firstName,
          "last_name": profile.lastName,
          "user_name": profile.userName,
          "birthday": profile.birthday,
          "address": profile.address,
          "city": profile.city,
          "state": profile.state,
          "country": profile.country,
          "contact_no": profile.contactNo,
          "categories": profile.categories,
          "current_education": profile.currentEducation,
          "area_of_interest": profile.areaOfInterest,
          "language": profile.language,
          "university_name": profile.universityName,
          "school_name": profile.schoolName,
          "established_on": profile.estdOn,
          "user_id": profile.userId,
          "about": profile.about,
          "social_fb": profile.socialFb,
          "social_li": profile.socialLi,
          "social_tw": profile.socialTw,
          "status": true
        }
      }, headers: {
        'Authorization': 'Bearer ' + token
      });
}
