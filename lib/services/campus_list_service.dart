import 'package:Unihub/util/constants.dart';
import 'package:http/http.dart' as http;
import 'package:Unihub/model/campus.dart';

class CampusListService {
  static var client = http.Client();

  static Future<Campus> fetchCampusList(String token, String universityId) async {
    var response = await client.get(
      Uri.parse(apiUrl + "vendor/campus?university=$universityId"),
        headers: {
          'Authorization': 'Bearer ' + token
        }
    );

    var jsonString = response.body;
    print(jsonString);
    return campusFromJson(jsonString);
  }
}
