import 'package:Unihub/util/constants.dart';
import 'package:get/get_connect/connect.dart';

class LogoutService extends GetConnect {
  Future<Response> fetchLogoutInfo(String token) =>
      post(apiUrl + "auth/logout", null,
          headers: {'Authorization': 'Bearer ' + token});
}
