import 'package:Unihub/util/constants.dart';
import 'package:Unihub/model/lab.dart';
import 'package:http/http.dart' as http;

class LabListService {
  static var client = http.Client();

  static Future<Lab> fetchLabList(String token) async {
    var response = await client.get(
      Uri.parse(apiUrl + "vendor/lab"),
        headers: {
          'Authorization': 'Bearer ' + token
        }
    );

    var jsonString = response.body;
    print(jsonString);
    return labFromJson(jsonString);
  }
}
