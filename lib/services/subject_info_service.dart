import 'package:Unihub/model/subject_info.dart';
import 'package:Unihub/util/constants.dart';
import 'package:http/http.dart' as http;

class SubjectInfoService {
  static var client = http.Client();

  static Future<SubjectInfo> fetchSubjectInfo(String id) async {
    var response = await client.get(
      Uri.parse(apiUrl + "vendor/subject/" + id),
    );

    var jsonString = response.body;
    print(jsonString);
    return subjectInfoFromJson(jsonString);
  }
}
