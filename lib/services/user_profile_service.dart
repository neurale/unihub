import 'package:Unihub/util/constants.dart';
import 'package:get/get_connect/connect.dart';
import 'package:Unihub/model/user_profile.dart';

class UserProfileService extends GetConnect {
  Future<UserProfile> getUser(String token) async =>
      await get(apiUrl + "auth/profile",
              headers: {'Authorization': 'Bearer ' + token})
          .then((response) => userProfileFromJson(response.bodyString))
          .catchError((e) => null);
}
