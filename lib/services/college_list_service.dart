import 'package:Unihub/util/constants.dart';
import 'package:http/http.dart' as http;
import 'package:Unihub/model/college.dart';

class CollegeListService {
  static var client = http.Client();

  static Future<College> fetchCampusList(String token, String universityId) async {
    var response = await client.get(
      Uri.parse(apiUrl + "vendor/college?university=$universityId"),
      headers: {'Authorization': 'Bearer ' + token},
    );

    var jsonString = response.body;
    print(jsonString);
    return collegeFromJson(jsonString);
  }
}
