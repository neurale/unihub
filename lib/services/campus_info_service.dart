import 'package:Unihub/util/constants.dart';
import 'package:http/http.dart' as http;
import 'package:Unihub/model/campus_info.dart';

class CampusInfoService {
  static var client = http.Client();

  static Future<CampusInfo> fetchCampusInfo(String id) async {
    var response = await client.get(
      Uri.parse(apiUrl + "vendor/campus/" + id),
    );

    var jsonString = response.body;
    print(jsonString);
    return campusInfoFromJson(jsonString);
  }
}
