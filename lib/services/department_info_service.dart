import 'package:Unihub/model/department_info.dart';
import 'package:Unihub/util/constants.dart';
import 'package:http/http.dart' as http;

class DepartmentInfoService {
  static var client = http.Client();

  static Future<DepartmentInfo> fetchDepartmentInfo(String id) async {
    var response = await client.get(
      Uri.parse(apiUrl + "vendor/department/" + id),
    );

    var jsonString = response.body;
    print(jsonString);
    return departmentInfoFromJson(jsonString);
  }
}