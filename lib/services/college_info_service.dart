import 'package:Unihub/util/constants.dart';
import 'package:http/http.dart' as http;
import 'package:Unihub/model/college_info.dart';

class CollegeInfoService {
  static var client = http.Client();

  static Future<CollegeInfo> fetchCollegeInfo(String id) async {
    var response = await client.get(
      Uri.parse(apiUrl + "vendor/college/" + id),
    );

    var jsonString = response.body;
    print(jsonString);
    return collegeInfoFromJson(jsonString);
  }
}
