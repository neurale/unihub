import 'package:Unihub/model/course.dart';
import 'package:Unihub/util/constants.dart';
import 'package:http/http.dart' as http;

class CourseListService {
  static var client = http.Client();

  static Future<Course> fetchCourseList(String token) async {
    var response = await client.get(
      Uri.parse(apiUrl + "vendor/course"),
        headers: {
          'Authorization': 'Bearer ' + token
        }
    );

    var jsonString = response.body;
    print(jsonString);
    return courseFromJson(jsonString);
  }
}
