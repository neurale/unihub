import 'package:Unihub/util/constants.dart';
import 'package:http/http.dart' as http;
import 'package:Unihub/model/course_info.dart';

class CourseInfoService {
  static var client = http.Client();

  static Future<CourseInfo> fetchCourseInfo(String id) async {
    var response = await client.get(
      Uri.parse(apiUrl + "vendor/course/" + id),
    );

    var jsonString = response.body;
    print(jsonString);
    return courseInfoFromJson(jsonString);
  }
}