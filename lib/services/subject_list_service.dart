import 'package:Unihub/model/subject.dart';
import 'package:Unihub/util/constants.dart';
import 'package:http/http.dart' as http;

class SubjectListService {
  static var client = http.Client();

  static Future<Subject> fetchSubjectList(String token) async {
    var response = await client.get(
      Uri.parse(apiUrl + "vendor/subject"),
        headers: {
          'Authorization': 'Bearer ' + token
        }
    );

    var jsonString = response.body;
    print(jsonString);
    return subjectFromJson(jsonString);
  }
}