import 'package:Unihub/util/constants.dart';
import 'package:get/get_connect/connect.dart';

class RefreshTokenService extends GetConnect {
  Future<Response> fetchRefreshToken(String token) =>
      post(apiUrl + "auth/refresh", null,
          headers: {'Authorization': 'Bearer ' + token});
}
