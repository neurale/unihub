import 'package:Unihub/util/constants.dart';
import 'package:get/get_connect/connect.dart';

class LoginService extends GetConnect {
  Future<Response> fetchLoginInfo(String email, String password) =>
      post(apiUrl + "auth/login", {'email': email, 'password': password});
}
