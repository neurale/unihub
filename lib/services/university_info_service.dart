import 'package:Unihub/util/constants.dart';
import 'package:http/http.dart' as http;
import 'package:Unihub/model/university_info.dart';

class UniversityInfoService {
  static var client = http.Client();

  static Future<UniversityInfo> fetchUniversityInfo(String id) async {
    var response = await client.get(
      Uri.parse(apiUrl + "vendor/university/" + id),
      // headers: {'Authorization': idToken},
    );

    var jsonString = response.body;
    print(jsonString);
    return universityInfoFromJson(jsonString);
  }
}
