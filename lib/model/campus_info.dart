// To parse this JSON data, do
//
//     final campusInfo = campusInfoFromJson(jsonString);

import 'dart:convert';

import 'lab.dart';

CampusInfo campusInfoFromJson(String str) => CampusInfo.fromJson(json.decode(str));

String campusInfoToJson(CampusInfo data) => json.encode(data.toJson());

class CampusInfo {
  CampusInfo({
    this.status,
    this.data,
  });

  bool status;
  Data data;

  factory CampusInfo.fromJson(Map<String, dynamic> json) => CampusInfo(
        status: json["status"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "data": data.toJson(),
      };
}

class Data {
  Data({
    this.id,
    this.campusName,
    this.tagline,
    this.summary,
    this.address,
    this.street,
    this.city,
    this.country,
    this.contactNo,
    this.location,
    this.profileImage,
    this.wallImage,
    this.specialties,
    this.dormAvailability,
    this.roomCount,
    this.universityId,
    this.status,
    this.university,
  });

  int id;
  String campusName;
  String tagline;
  String summary;
  String address;
  String street;
  String city;
  String country;
  List<String> contactNo;
  String location;
  String profileImage;
  dynamic wallImage;
  String specialties;
  int dormAvailability;
  int roomCount;
  int universityId;
  int status;
  University university;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"],
        campusName: json["campus_name"],
        tagline: json["tagline"],
        summary: json["summary"],
        address: json["address"],
        street: json["street"],
        city: json["city"],
        country: json["country"],
        contactNo: List<String>.from(json["contact_no"].map((x) => x)),
        location: json["location"],
        profileImage: json["profile_image"],
        wallImage: json["wall_image"],
        specialties: json["specialties"],
        dormAvailability: json["dorm_availability"],
        roomCount: json["room_count"],
        universityId: json["university_id"],
        status: json["status"],
        university: University.fromJson(json["university"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "campus_name": campusName,
        "tagline": tagline,
        "summary": summary,
        "address": address,
        "street": street,
        "city": city,
        "country": country,
        "contact_no": List<dynamic>.from(contactNo.map((x) => x)),
        "location": location,
        "profile_image": profileImage,
        "wall_image": wallImage,
        "specialties": specialties,
        "dorm_availability": dormAvailability,
        "room_count": roomCount,
        "university_id": universityId,
        "status": status,
        "university": university.toJson(),
      };
}

class Pivot {
  Pivot({
    this.departmentId,
    this.labId,
  });

  int departmentId;
  int labId;

  factory Pivot.fromJson(Map<String, dynamic> json) => Pivot(
        departmentId: json["department_id"],
        labId: json["lab_id"],
      );

  Map<String, dynamic> toJson() => {
        "department_id": departmentId,
        "lab_id": labId,
      };
}
