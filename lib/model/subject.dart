// To parse this JSON data, do
//
//     final subject = subjectFromJson(jsonString);

import 'dart:convert';

Subject subjectFromJson(String str) => Subject.fromJson(json.decode(str));

String subjectToJson(Subject data) => json.encode(data.toJson());

class Subject {
  Subject({
    this.currentPage,
    this.data,
    this.firstPageUrl,
    this.from,
    this.lastPage,
    this.lastPageUrl,
    this.nextPageUrl,
    this.path,
    this.perPage,
    this.prevPageUrl,
    this.to,
    this.total,
  });

  int currentPage;
  List<Datum> data;
  String firstPageUrl;
  int from;
  int lastPage;
  String lastPageUrl;
  dynamic nextPageUrl;
  String path;
  String perPage;
  dynamic prevPageUrl;
  int to;
  int total;

  factory Subject.fromJson(Map<String, dynamic> json) => Subject(
        currentPage: json["current_page"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        firstPageUrl: json["first_page_url"],
        from: json["from"],
        lastPage: json["last_page"],
        lastPageUrl: json["last_page_url"],
        nextPageUrl: json["next_page_url"],
        path: json["path"],
        perPage: json["per_page"],
        prevPageUrl: json["prev_page_url"],
        to: json["to"],
        total: json["total"],
      );

  Map<String, dynamic> toJson() => {
        "current_page": currentPage,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "first_page_url": firstPageUrl,
        "from": from,
        "last_page": lastPage,
        "last_page_url": lastPageUrl,
        "next_page_url": nextPageUrl,
        "path": path,
        "per_page": perPage,
        "prev_page_url": prevPageUrl,
        "to": to,
        "total": total,
      };
}

class Datum {
  Datum({
    this.id,
    this.subjectName,
    this.tagline,
    this.summary,
    this.collegeId,
    this.departmentId,
    this.courseId,
    this.syllabus,
    this.creditPoints,
    this.status,
    this.courses,
  });

  int id;
  String subjectName;
  String tagline;
  String summary;
  dynamic collegeId;
  dynamic departmentId;
  dynamic courseId;
  dynamic syllabus;
  dynamic creditPoints;
  int status;
  List<Course> courses;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        subjectName: json["subject_name"],
        tagline: json["tagline"],
        summary: json["summary"],
        collegeId: json["college_id"],
        departmentId: json["department_id"],
        courseId: json["course_id"],
        syllabus: json["syllabus"],
        creditPoints: json["credit_points"],
        status: json["status"],
        courses: List<Course>.from(json["courses"].map((x) => Course.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "subject_name": subjectName,
        "tagline": tagline,
        "summary": summary,
        "college_id": collegeId,
        "department_id": departmentId,
        "course_id": courseId,
        "syllabus": syllabus,
        "credit_points": creditPoints,
        "status": status,
        "courses": List<dynamic>.from(courses.map((x) => x.toJson())),
      };
}

class Course {
  Course({
    this.id,
    this.courseName,
    this.tagline,
    this.summary,
    this.collegeId,
    this.departmentId,
    this.courseFee,
    this.careers,
    this.moreInfo,
    this.local,
    this.foreign,
    this.status,
    this.universityId,
    this.profileImage,
    this.wallImage,
    this.pivot,
    this.department,
  });

  int id;
  String courseName;
  String tagline;
  String summary;
  dynamic collegeId;
  int departmentId;
  String courseFee;
  List<String> careers;
  dynamic moreInfo;
  dynamic local;
  dynamic foreign;
  int status;
  dynamic universityId;
  dynamic profileImage;
  dynamic wallImage;
  Pivot pivot;
  Department department;

  factory Course.fromJson(Map<String, dynamic> json) => Course(
        id: json["id"],
        courseName: json["course_name"],
        tagline: json["tagline"],
        summary: json["summary"],
        collegeId: json["college_id"],
        departmentId: json["department_id"],
        courseFee: json["course_fee"],
        careers: List<String>.from(json["careers"].map((x) => x)),
        moreInfo: json["more_info"],
        local: json["local"],
        foreign: json["foreign"],
        status: json["status"],
        universityId: json["university_id"],
        profileImage: json["profile_image"],
        wallImage: json["wall_image"],
        pivot: Pivot.fromJson(json["pivot"]),
        department: Department.fromJson(json["department"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "course_name": courseName,
        "tagline": tagline,
        "summary": summary,
        "college_id": collegeId,
        "department_id": departmentId,
        "course_fee": courseFee,
        "careers": List<dynamic>.from(careers.map((x) => x)),
        "more_info": moreInfo,
        "local": local,
        "foreign": foreign,
        "status": status,
        "university_id": universityId,
        "profile_image": profileImage,
        "wall_image": wallImage,
        "pivot": pivot.toJson(),
        "department": department.toJson(),
      };
}

class Department {
  Department({
    this.id,
    this.departmentName,
    this.tagline,
    this.summary,
    this.collegeId,
    this.contactNo,
    this.status,
    this.universityId,
    this.profileImage,
    this.wallImage,
    this.college,
  });

  int id;
  String departmentName;
  String tagline;
  String summary;
  int collegeId;
  List<String> contactNo;
  int status;
  int universityId;
  dynamic profileImage;
  dynamic wallImage;
  College college;

  factory Department.fromJson(Map<String, dynamic> json) => Department(
        id: json["id"],
        departmentName: json["department_name"],
        tagline: json["tagline"],
        summary: json["summary"],
        collegeId: json["college_id"],
        contactNo: List<String>.from(json["contact_no"].map((x) => x)),
        status: json["status"],
        universityId: json["university_id"],
        profileImage: json["profile_image"],
        wallImage: json["wall_image"],
        college: College.fromJson(json["college"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "department_name": departmentName,
        "tagline": tagline,
        "summary": summary,
        "college_id": collegeId,
        "contact_no": List<dynamic>.from(contactNo.map((x) => x)),
        "status": status,
        "university_id": universityId,
        "profile_image": profileImage,
        "wall_image": wallImage,
        "college": college.toJson(),
      };
}

class College {
  College({
    this.id,
    this.collegeName,
    this.tagline,
    this.summary,
    this.profileImage,
    this.wallImage,
    this.contactNo,
    this.status,
    this.universityId,
    this.university,
  });

  int id;
  String collegeName;
  String tagline;
  String summary;
  String profileImage;
  String wallImage;
  List<String> contactNo;
  int status;
  int universityId;
  University university;

  factory College.fromJson(Map<String, dynamic> json) => College(
        id: json["id"],
        collegeName: json["college_name"],
        tagline: json["tagline"],
        summary: json["summary"],
        profileImage: json["profile_image"],
        wallImage: json["wall_image"],
        contactNo: List<String>.from(json["contact_no"].map((x) => x)),
        status: json["status"],
        universityId: json["university_id"],
        university: University.fromJson(json["university"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "college_name": collegeName,
        "tagline": tagline,
        "summary": summary,
        "profile_image": profileImage,
        "wall_image": wallImage,
        "contact_no": List<dynamic>.from(contactNo.map((x) => x)),
        "status": status,
        "university_id": universityId,
        "university": university.toJson(),
      };
}

class University {
  University({
    this.id,
    this.universityName,
    this.tagline,
    this.summary,
    this.website,
    this.worldRank,
    this.countryRank,
    this.establishedOn,
    this.contactNo,
    this.address,
    this.street,
    this.city,
    this.country,
    this.location,
    this.speciality,
    this.profileImage,
    this.wallImage,
    this.galleryImage,
    this.staff,
    this.intStudents,
    this.universityType,
    this.status,
    this.locStudents,
  });

  int id;
  String universityName;
  String tagline;
  String summary;
  dynamic website;
  dynamic worldRank;
  dynamic countryRank;
  dynamic establishedOn;
  String contactNo;
  String address;
  dynamic street;
  String city;
  String country;
  dynamic location;
  dynamic speciality;
  String profileImage;
  dynamic wallImage;
  dynamic galleryImage;
  dynamic staff;
  dynamic intStudents;
  dynamic universityType;
  int status;
  dynamic locStudents;

  factory University.fromJson(Map<String, dynamic> json) => University(
        id: json["id"],
        universityName: json["university_name"],
        tagline: json["tagline"],
        summary: json["summary"],
        website: json["website"],
        worldRank: json["world_rank"],
        countryRank: json["country_rank"],
        establishedOn: json["established_on"],
        contactNo: json["contact_no"],
        address: json["address"],
        street: json["street"],
        city: json["city"],
        country: json["country"],
        location: json["location"],
        speciality: json["speciality"],
        profileImage: json["profile_image"],
        wallImage: json["wall_image"],
        galleryImage: json["gallery_image"],
        staff: json["staff"],
        intStudents: json["int_students"],
        universityType: json["university_type"],
        status: json["status"],
        locStudents: json["loc_students"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "university_name": universityName,
        "tagline": tagline,
        "summary": summary,
        "website": website,
        "world_rank": worldRank,
        "country_rank": countryRank,
        "established_on": establishedOn,
        "contact_no": contactNo,
        "address": address,
        "street": street,
        "city": city,
        "country": country,
        "location": location,
        "speciality": speciality,
        "profile_image": profileImage,
        "wall_image": wallImage,
        "gallery_image": galleryImage,
        "staff": staff,
        "int_students": intStudents,
        "university_type": universityType,
        "status": status,
        "loc_students": locStudents,
      };
}

class Pivot {
  Pivot({
    this.subjectId,
    this.courseId,
  });

  int subjectId;
  int courseId;

  factory Pivot.fromJson(Map<String, dynamic> json) => Pivot(
        subjectId: json["subject_id"],
        courseId: json["course_id"],
      );

  Map<String, dynamic> toJson() => {
        "subject_id": subjectId,
        "course_id": courseId,
      };
}
