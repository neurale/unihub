// To parse this JSON data, do
//
//     final campus = campusFromJson(jsonString);

import 'dart:convert';

Campus campusFromJson(String str) => Campus.fromJson(json.decode(str));

String campusToJson(Campus data) => json.encode(data.toJson());

class Campus {
  Campus({
    this.currentPage,
    this.data,
    this.firstPageUrl,
    this.from,
    this.lastPage,
    this.lastPageUrl,
    this.nextPageUrl,
    this.path,
    this.perPage,
    this.prevPageUrl,
    this.to,
    this.total,
  });

  int currentPage;
  List<Datum> data;
  String firstPageUrl;
  int from;
  int lastPage;
  String lastPageUrl;
  dynamic nextPageUrl;
  String path;
  String perPage;
  dynamic prevPageUrl;
  int to;
  int total;

  factory Campus.fromJson(Map<String, dynamic> json) => Campus(
        currentPage: json["current_page"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        firstPageUrl: json["first_page_url"],
        from: json["from"],
        lastPage: json["last_page"],
        lastPageUrl: json["last_page_url"],
        nextPageUrl: json["next_page_url"],
        path: json["path"],
        perPage: json["per_page"],
        prevPageUrl: json["prev_page_url"],
        to: json["to"],
        total: json["total"],
      );

  Map<String, dynamic> toJson() => {
        "current_page": currentPage,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "first_page_url": firstPageUrl,
        "from": from,
        "last_page": lastPage,
        "last_page_url": lastPageUrl,
        "next_page_url": nextPageUrl,
        "path": path,
        "per_page": perPage,
        "prev_page_url": prevPageUrl,
        "to": to,
        "total": total,
      };
}

class Datum {
  Datum({
    this.id,
    this.campusName,
    this.tagline,
    this.summary,
    this.address,
    this.street,
    this.city,
    this.country,
    this.contactNo,
    this.location,
    this.profileImage,
    this.wallImage,
    this.specialties,
    this.dormAvailability,
    this.roomCount,
    this.universityId,
    this.status,
    this.university,
  });

  int id;
  String campusName;
  String tagline;
  String summary;
  String address;
  String street;
  String city;
  String country;
  List<String> contactNo;
  String location;
  String profileImage;
  dynamic wallImage;
  String specialties;
  int dormAvailability;
  int roomCount;
  int universityId;
  int status;
  University university;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        campusName: json["campus_name"],
        tagline: json["tagline"],
        summary: json["summary"],
        address: json["address"],
        street: json["street"],
        city: json["city"],
        country: json["country"],
        contactNo: json["contact_no"] == null ? null : List<String>.from(json["contact_no"].map((x) => x)),
        location: json["location"],
        profileImage: json["profile_image"],
        wallImage: json["wall_image"],
        specialties: json["specialties"],
        dormAvailability: json["dorm_availability"],
        roomCount: json["room_count"],
        universityId: json["university_id"],
        status: json["status"],
        university: University.fromJson(json["university"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "campus_name": campusName,
        "tagline": tagline,
        "summary": summary,
        "address": address,
        "street": street,
        "city": city,
        "country": country,
        "contact_no": contactNo == null ? null : List<dynamic>.from(contactNo.map((x) => x)),
        "location": location,
        "profile_image": profileImage,
        "wall_image": wallImage,
        "specialties": specialties,
        "dorm_availability": dormAvailability,
        "room_count": roomCount,
        "university_id": universityId,
        "status": status,
        "university": university.toJson(),
      };
}

class University {
  University({
    this.id,
    this.universityName,
    this.tagline,
    this.summary,
    this.website,
    this.worldRank,
    this.countryRank,
    this.establishedOn,
    this.contactNo,
    this.address,
    this.street,
    this.city,
    this.country,
    this.location,
    this.speciality,
    this.profileImage,
    this.wallImage,
    this.galleryImage,
    this.staff,
    this.intStudents,
    this.universityType,
    this.status,
    this.locStudents,
    this.colleges,
  });

  int id;
  String universityName;
  String tagline;
  String summary;
  String website;
  String worldRank;
  String countryRank;
  DateTime establishedOn;
  List<String> contactNo;
  String address;
  String street;
  String city;
  String country;
  String location;
  String speciality;
  dynamic profileImage;
  String wallImage;
  List<String> galleryImage;
  int staff;
  int intStudents;
  String universityType;
  int status;
  int locStudents;
  List<College> colleges;

  factory University.fromJson(Map<String, dynamic> json) => University(
        id: json["id"],
        universityName: json["university_name"],
        tagline: json["tagline"],
        summary: json["summary"],
        website: json["website"],
        worldRank: json["world_rank"],
        countryRank: json["country_rank"],
        establishedOn: DateTime.parse(json["established_on"]),
        contactNo: List<String>.from(json["contact_no"].map((x) => x)),
        address: json["address"],
        street: json["street"],
        city: json["city"],
        country: json["country"],
        location: json["location"],
        speciality: json["speciality"],
        profileImage: json["profile_image"],
        wallImage: json["wall_image"],
        galleryImage: List<String>.from(json["gallery_image"].map((x) => x)),
        staff: json["staff"],
        intStudents: json["int_students"],
        universityType: json["university_type"],
        status: json["status"],
        locStudents: json["loc_students"],
        colleges: List<College>.from(json["colleges"].map((x) => College.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "university_name": universityName,
        "tagline": tagline,
        "summary": summary,
        "website": website,
        "world_rank": worldRank,
        "country_rank": countryRank,
        "established_on":
            "${establishedOn.year.toString().padLeft(4, '0')}-${establishedOn.month.toString().padLeft(2, '0')}-${establishedOn.day.toString().padLeft(2, '0')}",
        "contact_no": List<dynamic>.from(contactNo.map((x) => x)),
        "address": address,
        "street": street,
        "city": city,
        "country": country,
        "location": location,
        "speciality": speciality,
        "profile_image": profileImage,
        "wall_image": wallImage,
        "gallery_image": List<dynamic>.from(galleryImage.map((x) => x)),
        "staff": staff,
        "int_students": intStudents,
        "university_type": universityType,
        "status": status,
        "loc_students": locStudents,
        "colleges": List<dynamic>.from(colleges.map((x) => x.toJson())),
      };
}

class College {
  College({
    this.id,
    this.collegeName,
    this.tagline,
    this.summary,
    this.profileImage,
    this.wallImage,
    this.contactNo,
    this.status,
    this.universityId,
    this.laravelThroughKey,
    this.departments,
  });

  int id;
  String collegeName;
  String tagline;
  String summary;
  String profileImage;
  String wallImage;
  List<String> contactNo;
  int status;
  int universityId;
  int laravelThroughKey;
  List<Department> departments;

  factory College.fromJson(Map<String, dynamic> json) => College(
        id: json["id"],
        collegeName: json["college_name"],
        tagline: json["tagline"],
        summary: json["summary"],
        profileImage: json["profile_image"],
        wallImage: json["wall_image"],
        contactNo: List<String>.from(json["contact_no"].map((x) => x)),
        status: json["status"],
        universityId: json["university_id"],
        laravelThroughKey: json["laravel_through_key"],
        departments: List<Department>.from(json["departments"].map((x) => Department.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "college_name": collegeName,
        "tagline": tagline,
        "summary": summary,
        "profile_image": profileImage,
        "wall_image": wallImage,
        "contact_no": List<dynamic>.from(contactNo.map((x) => x)),
        "status": status,
        "university_id": universityId,
        "laravel_through_key": laravelThroughKey,
        "departments": List<dynamic>.from(departments.map((x) => x.toJson())),
      };
}

class Department {
  Department({
    this.id,
    this.departmentName,
    this.tagline,
    this.summary,
    this.collegeId,
    this.contactNo,
    this.status,
    this.universityId,
    this.profileImage,
    this.wallImage,
    this.courses,
    this.labs,
  });

  int id;
  String departmentName;
  String tagline;
  String summary;
  int collegeId;
  List<String> contactNo;
  int status;
  int universityId;
  dynamic profileImage;
  dynamic wallImage;
  List<Course> courses;
  List<Lab> labs;

  factory Department.fromJson(Map<String, dynamic> json) => Department(
        id: json["id"],
        departmentName: json["department_name"],
        tagline: json["tagline"],
        summary: json["summary"],
        collegeId: json["college_id"],
        contactNo: List<String>.from(json["contact_no"].map((x) => x)),
        status: json["status"],
        universityId: json["university_id"],
        profileImage: json["profile_image"],
        wallImage: json["wall_image"],
        courses: List<Course>.from(json["courses"].map((x) => Course.fromJson(x))),
        labs: List<Lab>.from(json["labs"].map((x) => Lab.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "department_name": departmentName,
        "tagline": tagline,
        "summary": summary,
        "college_id": collegeId,
        "contact_no": List<dynamic>.from(contactNo.map((x) => x)),
        "status": status,
        "university_id": universityId,
        "profile_image": profileImage,
        "wall_image": wallImage,
        "courses": List<dynamic>.from(courses.map((x) => x.toJson())),
        "labs": List<dynamic>.from(labs.map((x) => x.toJson())),
      };
}

class Course {
  Course({
    this.id,
    this.courseName,
    this.tagline,
    this.summary,
    this.collegeId,
    this.departmentId,
    this.courseFee,
    this.careers,
    this.moreInfo,
    this.local,
    this.foreign,
    this.status,
    this.universityId,
    this.profileImage,
    this.wallImage,
    this.subjects,
  });

  int id;
  String courseName;
  String tagline;
  String summary;
  int collegeId;
  int departmentId;
  String courseFee;
  List<Career> careers;
  dynamic moreInfo;
  String local;
  String foreign;
  int status;
  int universityId;
  dynamic profileImage;
  dynamic wallImage;
  List<dynamic> subjects;

  factory Course.fromJson(Map<String, dynamic> json) => Course(
        id: json["id"],
        courseName: json["course_name"],
        tagline: json["tagline"],
        summary: json["summary"],
        collegeId: json["college_id"] == null ? null : json["college_id"],
        departmentId: json["department_id"],
        courseFee: json["course_fee"],
        careers: List<Career>.from(json["careers"].map((x) => careerValues.map[x])),
        moreInfo: json["more_info"],
        local: json["local"],
        foreign: json["foreign"],
        status: json["status"],
        universityId: json["university_id"] == null ? null : json["university_id"],
        profileImage: json["profile_image"],
        wallImage: json["wall_image"],
        subjects: List<dynamic>.from(json["subjects"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "course_name": courseName,
        "tagline": tagline,
        "summary": summary,
        "college_id": collegeId == null ? null : collegeId,
        "department_id": departmentId,
        "course_fee": courseFee,
        "careers": List<dynamic>.from(careers.map((x) => careerValues.reverse[x])),
        "more_info": moreInfo,
        "local": local,
        "foreign": foreign,
        "status": status,
        "university_id": universityId == null ? null : universityId,
        "profile_image": profileImage,
        "wall_image": wallImage,
        "subjects": List<dynamic>.from(subjects.map((x) => x)),
      };
}

enum Career { SE, DEVELOPER, NETWORK, ENGINEER }

final careerValues = EnumValues(
    {"Developer": Career.DEVELOPER, "Engineer": Career.ENGINEER, "Network": Career.NETWORK, "SE": Career.SE});

class Lab {
  Lab({
    this.id,
    this.labName,
    this.tagline,
    this.summary,
    this.status,
    this.profileImage,
    this.wallImage,
    this.departmentId,
    this.universityId,
    this.campusId,
    this.pivot,
  });

  int id;
  String labName;
  String tagline;
  String summary;
  int status;
  dynamic profileImage;
  dynamic wallImage;
  int departmentId;
  int universityId;
  int campusId;
  Pivot pivot;

  factory Lab.fromJson(Map<String, dynamic> json) => Lab(
        id: json["id"],
        labName: json["lab_name"],
        tagline: json["tagline"],
        summary: json["summary"],
        status: json["status"],
        profileImage: json["profile_image"],
        wallImage: json["wall_image"],
        departmentId: json["department_id"],
        universityId: json["university_id"],
        campusId: json["campus_id"],
        pivot: Pivot.fromJson(json["pivot"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "lab_name": labName,
        "tagline": tagline,
        "summary": summary,
        "status": status,
        "profile_image": profileImage,
        "wall_image": wallImage,
        "department_id": departmentId,
        "university_id": universityId,
        "campus_id": campusId,
        "pivot": pivot.toJson(),
      };
}

class Pivot {
  Pivot({
    this.departmentId,
    this.labId,
  });

  int departmentId;
  int labId;

  factory Pivot.fromJson(Map<String, dynamic> json) => Pivot(
        departmentId: json["department_id"],
        labId: json["lab_id"],
      );

  Map<String, dynamic> toJson() => {
        "department_id": departmentId,
        "lab_id": labId,
      };
}

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
