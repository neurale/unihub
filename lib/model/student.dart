// To parse this JSON data, do
//
//     final student = studentFromJson(jsonString);

import 'dart:convert';

Student studentFromJson(String str) => Student.fromJson(json.decode(str));

String studentToJson(Student data) => json.encode(data.toJson());

class Student {
  Student({
    this.currentPage,
    this.data,
    this.firstPageUrl,
    this.from,
    this.lastPage,
    this.lastPageUrl,
    this.links,
    this.nextPageUrl,
    this.path,
    this.perPage,
    this.prevPageUrl,
    this.to,
    this.total,
  });

  int currentPage;
  List<Datum> data;
  String firstPageUrl;
  int from;
  int lastPage;
  String lastPageUrl;
  List<Link> links;
  String nextPageUrl;
  String path;
  String perPage;
  dynamic prevPageUrl;
  int to;
  int total;

  factory Student.fromJson(Map<String, dynamic> json) => Student(
        currentPage: json["current_page"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        firstPageUrl: json["first_page_url"],
        from: json["from"],
        lastPage: json["last_page"],
        lastPageUrl: json["last_page_url"],
        links: List<Link>.from(json["links"].map((x) => Link.fromJson(x))),
        nextPageUrl: json["next_page_url"],
        path: json["path"],
        perPage: json["per_page"],
        prevPageUrl: json["prev_page_url"],
        to: json["to"],
        total: json["total"],
      );

  Map<String, dynamic> toJson() => {
        "current_page": currentPage,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "first_page_url": firstPageUrl,
        "from": from,
        "last_page": lastPage,
        "last_page_url": lastPageUrl,
        "links": List<dynamic>.from(links.map((x) => x.toJson())),
        "next_page_url": nextPageUrl,
        "path": path,
        "per_page": perPage,
        "prev_page_url": prevPageUrl,
        "to": to,
        "total": total,
      };
}

class Datum {
  Datum({
    this.id,
    this.studentName,
    this.userName,
    this.dob,
    this.email,
    this.country,
    this.city,
    this.contactNo,
    this.language,
    this.university,
    this.secEducationLevel,
    this.school,
    this.priEducationLevel,
    this.interestedArea,
    this.interestedSubject,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  String studentName;
  String userName;
  String dob;
  String email;
  String country;
  String city;
  List<ContactNo> contactNo;
  String language;
  String university;
  String secEducationLevel;
  String school;
  String priEducationLevel;
  dynamic interestedArea;
  dynamic interestedSubject;
  DateTime createdAt;
  DateTime updatedAt;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        studentName: json["student_name"],
        userName: json["user_name"] == null ? null : json["user_name"],
        dob: json["dob"] == null ? null : json["dob"],
        email: json["email"],
        country: json["country"] == null ? null : json["country"],
        city: json["city"] == null ? null : json["city"],
        contactNo: json["contact_no"] == null
            ? null
            : List<ContactNo>.from(json["contact_no"].map((x) => ContactNo.fromJson(x))),
        language: json["language"] == null ? null : json["language"],
        university: json["university"] == null ? null : json["university"],
        secEducationLevel: json["sec_education_level"] == null ? null : json["sec_education_level"],
        school: json["school"] == null ? null : json["school"],
        priEducationLevel: json["pri_education_level"] == null ? null : json["pri_education_level"],
        interestedArea: json["interested_area"],
        interestedSubject: json["interested_subject"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "student_name": studentName,
        "user_name": userName == null ? null : userName,
        "dob": dob == null ? null : dob,
        "email": email,
        "country": country == null ? null : country,
        "city": city == null ? null : city,
        "contact_no": contactNo == null ? null : List<dynamic>.from(contactNo.map((x) => x.toJson())),
        "language": language == null ? null : language,
        "university": university == null ? null : university,
        "sec_education_level": secEducationLevel == null ? null : secEducationLevel,
        "school": school == null ? null : school,
        "pri_education_level": priEducationLevel == null ? null : priEducationLevel,
        "interested_area": interestedArea,
        "interested_subject": interestedSubject,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
      };
}

class ContactNo {
  ContactNo({
    this.phoneNumber,
  });

  String phoneNumber;

  factory ContactNo.fromJson(Map<String, dynamic> json) => ContactNo(
        phoneNumber: json["phone_number"],
      );

  Map<String, dynamic> toJson() => {
        "phone_number": phoneNumber,
      };
}

class Link {
  Link({
    this.url,
    this.label,
    this.active,
  });

  String url;
  String label;
  bool active;

  factory Link.fromJson(Map<String, dynamic> json) => Link(
        url: json["url"] == null ? null : json["url"],
        label: json["label"],
        active: json["active"],
      );

  Map<String, dynamic> toJson() => {
        "url": url == null ? null : url,
        "label": label,
        "active": active,
      };
}
