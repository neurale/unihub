// To parse this JSON data, do
//
//     final userProfile = userProfileFromJson(jsonString);

import 'dart:convert';

UserProfile userProfileFromJson(String str) => UserProfile.fromJson(json.decode(str));

String userProfileToJson(UserProfile data) => json.encode(data.toJson());

class UserProfile {
  UserProfile({
    this.id,
    this.name,
    this.email,
    this.emailVerifiedAt,
    this.userType,
    this.status,
    this.role,
    this.university,
    this.profile,
    this.permissions,
  });

  int id;
  String name;
  String email;
  dynamic emailVerifiedAt;
  int userType;
  int status;
  String role;
  dynamic university;
  Profile profile;
  List<dynamic> permissions;

  factory UserProfile.fromJson(Map<String, dynamic> json) => UserProfile(
        id: json["id"],
        name: json["name"],
        email: json["email"],
        emailVerifiedAt: json["email_verified_at"],
        userType: json["user_type"],
        status: json["status"],
        role: json["role"],
        university: json["university"],
        profile: Profile.fromJson(json["profile"]),
        permissions: List<dynamic>.from(json["permissions"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "email": email,
        "email_verified_at": emailVerifiedAt,
        "user_type": userType,
        "status": status,
        "role": role,
        "university": university,
        "profile": profile.toJson(),
        "permissions": List<dynamic>.from(permissions.map((x) => x)),
      };
}

class Profile {
  Profile({
    this.id,
    this.firstName,
    this.lastName,
    this.userName,
    this.birthday,
    this.address,
    this.city,
    this.state,
    this.country,
    this.contactNo,
    this.profilePicture,
    this.language,
    this.categories,
    this.currentEducation,
    this.areaOfInterest,
    this.universityId,
    this.schoolName,
    this.about,
    this.socialFb,
    this.socialTw,
    this.socialLi,
    this.estdOn,
    this.userId,
    this.createdAt,
    this.updatedAt,
    this.universityName,
  });

  int id;
  String firstName;
  String lastName;
  String userName;
  DateTime birthday;
  String address;
  String city;
  String state;
  String country;
  dynamic contactNo;
  dynamic profilePicture;
  List<String> language;
  List<String> categories;
  List<CurrentEducation> currentEducation;
  List<AreaOfInterest> areaOfInterest;
  dynamic universityId;
  dynamic schoolName;
  dynamic about;
  dynamic socialFb;
  dynamic socialTw;
  dynamic socialLi;
  dynamic estdOn;
  int userId;
  DateTime createdAt;
  DateTime updatedAt;
  dynamic universityName;

  factory Profile.fromJson(Map<String, dynamic> json) => Profile(
        id: json["id"],
        firstName: json["first_name"],
        lastName: json["last_name"],
        userName: json["user_name"],
        birthday: DateTime.parse(json["birthday"]),
        address: json["address"],
        city: json["city"],
        state: json["state"],
        country: json["country"],
        contactNo: json["contact_no"],
        profilePicture: json["profile_picture"],
        language: List<String>.from(json["language"].map((x) => x)),
        categories: List<String>.from(json["categories"].map((x) => x)),
        currentEducation:
            List<CurrentEducation>.from(json["current_education"].map((x) => CurrentEducation.fromJson(x))),
        areaOfInterest: List<AreaOfInterest>.from(json["area_of_interest"].map((x) => AreaOfInterest.fromJson(x))),
        universityId: json["university_id"],
        schoolName: json["school_name"],
        about: json["about"],
        socialFb: json["social_fb"],
        socialTw: json["social_tw"],
        socialLi: json["social_li"],
        estdOn: json["estd_on"],
        userId: json["user_id"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        universityName: json["university_name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "first_name": firstName,
        "last_name": lastName,
        "user_name": userName,
        "birthday":
            "${birthday.year.toString().padLeft(4, '0')}-${birthday.month.toString().padLeft(2, '0')}-${birthday.day.toString().padLeft(2, '0')}",
        "address": address,
        "city": city,
        "state": state,
        "country": country,
        "contact_no": contactNo,
        "profile_picture": profilePicture,
        "language": List<dynamic>.from(language.map((x) => x)),
        "categories": List<dynamic>.from(categories.map((x) => x)),
        "current_education": List<dynamic>.from(currentEducation.map((x) => x.toJson())),
        "area_of_interest": List<dynamic>.from(areaOfInterest.map((x) => x.toJson())),
        "university_id": universityId,
        "school_name": schoolName,
        "about": about,
        "social_fb": socialFb,
        "social_tw": socialTw,
        "social_li": socialLi,
        "estd_on": estdOn,
        "user_id": userId,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "university_name": universityName,
      };
}

class AreaOfInterest {
  AreaOfInterest({
    this.city,
    this.location,
  });

  List<String> city;
  String location;

  factory AreaOfInterest.fromJson(Map<String, dynamic> json) => AreaOfInterest(
        city: List<String>.from(json["city"].map((x) => x)),
        location: json["location"],
      );

  Map<String, dynamic> toJson() => {
        "city": List<dynamic>.from(city.map((x) => x)),
        "location": location,
      };
}

class CurrentEducation {
  CurrentEducation({
    this.university,
    this.educationLevel,
  });

  String university;
  String educationLevel;

  factory CurrentEducation.fromJson(Map<String, dynamic> json) => CurrentEducation(
        university: json["university"],
        educationLevel: json["education_level"],
      );

  Map<String, dynamic> toJson() => {
        "university": university,
        "education_level": educationLevel,
      };
}
