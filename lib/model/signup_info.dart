// To parse this JSON data, do
//
//     final signupInfo = signupInfoFromJson(jsonString);

import 'dart:convert';

SignupInfo signupInfoFromJson(String str) =>
    SignupInfo.fromJson(json.decode(str));

String signupInfoToJson(SignupInfo data) => json.encode(data.toJson());

class SignupInfo {
  SignupInfo({
    this.message,
    this.code,
    this.statusCode,
  });

  String message;
  int code;
  int statusCode;

  factory SignupInfo.fromJson(Map<String, dynamic> json) => SignupInfo(
        message: json["message"],
        code: json["code"],
        statusCode: json["status_code"],
      );

  Map<String, dynamic> toJson() => {
        "message": message,
        "code": code,
        "status_code": statusCode,
      };
}
