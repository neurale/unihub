// To parse this JSON data, do
//
//     final courseInfo = courseInfoFromJson(jsonString);

import 'dart:convert';

CourseInfo courseInfoFromJson(String str) => CourseInfo.fromJson(json.decode(str));

String courseInfoToJson(CourseInfo data) => json.encode(data.toJson());

class CourseInfo {
  CourseInfo({
    this.status,
    this.data,
  });

  bool status;
  Data data;

  factory CourseInfo.fromJson(Map<String, dynamic> json) => CourseInfo(
        status: json["status"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "data": data.toJson(),
      };
}

class Data {
  Data({
    this.id,
    this.courseName,
    this.tagline,
    this.summary,
    this.collegeId,
    this.departmentId,
    this.courseFee,
    this.careers,
    this.moreInfo,
    this.local,
    this.foreign,
    this.status,
    this.universityId,
    this.profileImage,
    this.wallImage,
    this.department,
  });

  int id;
  String courseName;
  String tagline;
  String summary;
  int collegeId;
  int departmentId;
  String courseFee;
  List<String> careers;
  dynamic moreInfo;
  String local;
  String foreign;
  int status;
  int universityId;
  dynamic profileImage;
  dynamic wallImage;
  Department department;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"],
        courseName: json["course_name"],
        tagline: json["tagline"],
        summary: json["summary"],
        collegeId: json["college_id"],
        departmentId: json["department_id"],
        courseFee: json["course_fee"],
        careers: List<String>.from(json["careers"].map((x) => x)),
        moreInfo: json["more_info"],
        local: json["local"],
        foreign: json["foreign"],
        status: json["status"],
        universityId: json["university_id"],
        profileImage: json["profile_image"],
        wallImage: json["wall_image"],
        department: Department.fromJson(json["department"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "course_name": courseName,
        "tagline": tagline,
        "summary": summary,
        "college_id": collegeId,
        "department_id": departmentId,
        "course_fee": courseFee,
        "careers": List<dynamic>.from(careers.map((x) => x)),
        "more_info": moreInfo,
        "local": local,
        "foreign": foreign,
        "status": status,
        "university_id": universityId,
        "profile_image": profileImage,
        "wall_image": wallImage,
        "department": department.toJson(),
      };
}

class Department {
  Department({
    this.id,
    this.departmentName,
    this.tagline,
    this.summary,
    this.collegeId,
    this.contactNo,
    this.status,
    this.universityId,
    this.profileImage,
    this.wallImage,
    this.college,
  });

  int id;
  String departmentName;
  String tagline;
  String summary;
  int collegeId;
  List<String> contactNo;
  int status;
  int universityId;
  dynamic profileImage;
  dynamic wallImage;
  College college;

  factory Department.fromJson(Map<String, dynamic> json) => Department(
        id: json["id"],
        departmentName: json["department_name"],
        tagline: json["tagline"],
        summary: json["summary"],
        collegeId: json["college_id"],
        contactNo: List<String>.from(json["contact_no"].map((x) => x)),
        status: json["status"],
        universityId: json["university_id"],
        profileImage: json["profile_image"],
        wallImage: json["wall_image"],
        college: College.fromJson(json["college"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "department_name": departmentName,
        "tagline": tagline,
        "summary": summary,
        "college_id": collegeId,
        "contact_no": List<dynamic>.from(contactNo.map((x) => x)),
        "status": status,
        "university_id": universityId,
        "profile_image": profileImage,
        "wall_image": wallImage,
        "college": college.toJson(),
      };
}

class College {
  College({
    this.id,
    this.collegeName,
    this.tagline,
    this.summary,
    this.profileImage,
    this.wallImage,
    this.contactNo,
    this.status,
    this.universityId,
    this.university,
  });

  int id;
  String collegeName;
  String tagline;
  String summary;
  String profileImage;
  String wallImage;
  List<String> contactNo;
  int status;
  int universityId;
  University university;

  factory College.fromJson(Map<String, dynamic> json) => College(
        id: json["id"],
        collegeName: json["college_name"],
        tagline: json["tagline"],
        summary: json["summary"],
        profileImage: json["profile_image"],
        wallImage: json["wall_image"],
        contactNo: List<String>.from(json["contact_no"].map((x) => x)),
        status: json["status"],
        universityId: json["university_id"],
        university: University.fromJson(json["university"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "college_name": collegeName,
        "tagline": tagline,
        "summary": summary,
        "profile_image": profileImage,
        "wall_image": wallImage,
        "contact_no": List<dynamic>.from(contactNo.map((x) => x)),
        "status": status,
        "university_id": universityId,
        "university": university.toJson(),
      };
}

class University {
  University({
    this.id,
    this.universityName,
    this.tagline,
    this.summary,
    this.website,
    this.worldRank,
    this.countryRank,
    this.establishedOn,
    this.contactNo,
    this.address,
    this.street,
    this.city,
    this.country,
    this.location,
    this.speciality,
    this.profileImage,
    this.wallImage,
    this.galleryImage,
    this.staff,
    this.intStudents,
    this.universityType,
    this.status,
    this.locStudents,
  });

  int id;
  String universityName;
  String tagline;
  String summary;
  String website;
  String worldRank;
  String countryRank;
  DateTime establishedOn;
  List<String> contactNo;
  String address;
  String street;
  String city;
  String country;
  String location;
  String speciality;
  dynamic profileImage;
  String wallImage;
  List<String> galleryImage;
  int staff;
  int intStudents;
  String universityType;
  int status;
  int locStudents;

  factory University.fromJson(Map<String, dynamic> json) => University(
        id: json["id"],
        universityName: json["university_name"],
        tagline: json["tagline"],
        summary: json["summary"],
        website: json["website"],
        worldRank: json["world_rank"],
        countryRank: json["country_rank"],
        establishedOn: DateTime.parse(json["established_on"]),
        contactNo: List<String>.from(json["contact_no"].map((x) => x)),
        address: json["address"],
        street: json["street"],
        city: json["city"],
        country: json["country"],
        location: json["location"],
        speciality: json["speciality"],
        profileImage: json["profile_image"],
        wallImage: json["wall_image"],
        galleryImage: List<String>.from(json["gallery_image"].map((x) => x)),
        staff: json["staff"],
        intStudents: json["int_students"],
        universityType: json["university_type"],
        status: json["status"],
        locStudents: json["loc_students"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "university_name": universityName,
        "tagline": tagline,
        "summary": summary,
        "website": website,
        "world_rank": worldRank,
        "country_rank": countryRank,
        "established_on":
            "${establishedOn.year.toString().padLeft(4, '0')}-${establishedOn.month.toString().padLeft(2, '0')}-${establishedOn.day.toString().padLeft(2, '0')}",
        "contact_no": List<dynamic>.from(contactNo.map((x) => x)),
        "address": address,
        "street": street,
        "city": city,
        "country": country,
        "location": location,
        "speciality": speciality,
        "profile_image": profileImage,
        "wall_image": wallImage,
        "gallery_image": List<dynamic>.from(galleryImage.map((x) => x)),
        "staff": staff,
        "int_students": intStudents,
        "university_type": universityType,
        "status": status,
        "loc_students": locStudents,
      };
}
