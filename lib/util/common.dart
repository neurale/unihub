import 'package:Unihub/view/theme/colors.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

List<String> getCampusNames(List campuses) {
  List<String> campusNames = [];
  if (campuses != null && campuses.length > 0) {
    campuses.forEach((element) => campusNames.add(element.campusName));
  }
  return campusNames;
}

showErrorToast(String message, {ToastGravity toastGravity}) {
  Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_SHORT,
      gravity: toastGravity ?? ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.red,
      textColor: Colors.white,
      fontSize: 14.0);
}

showWarningToast(String message, {ToastGravity toastGravity}) {
  Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_SHORT,
      gravity: toastGravity ?? ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: menuFontColor,
      textColor: Colors.white,
      fontSize: 14.0);
}

showInfoToast(String message, {ToastGravity toastGravity}) {
  Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_SHORT,
      gravity: toastGravity ?? ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.blue,
      textColor: Colors.white,
      fontSize: 14.0);
}
