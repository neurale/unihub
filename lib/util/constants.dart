const splashImagePath = "assets/images/splash.png";

enum UniListItemType {
  university,
  campus,
  department,
  college,
  course,
  lab,
  subject
}

const apiUrl = "http://gw.unibaz.qriousworld.com/api/";

const accessTokenKey = "TOKEN";