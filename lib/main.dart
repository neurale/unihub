import 'package:Unihub/services/localization.dart';
import 'package:Unihub/view/screens/splash.dart';
import 'package:Unihub/view/theme/colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'dart:ui';
import 'package:get_storage/get_storage.dart';

void main() async {
  await GetStorage.init();
  runApp(GetMaterialApp(
    theme: ThemeData(
      primaryColor: primaryGreen,
      fontFamily: 'Open Sans',
    ),
    home: SplashScreen(),
    locale: window.locale,
    fallbackLocale: LocalizationService.fallbackLocale,
    translations: LocalizationService(),
    debugShowCheckedModeBanner: false,
  ));
}
