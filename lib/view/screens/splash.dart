import 'dart:async';

import 'package:Unihub/util/constants.dart';
import 'package:Unihub/view/screens/home.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import 'login.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final _storage = GetStorage();

  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: 2), () {
      String token = _storage.read(accessTokenKey);
      if (token != null) {
        Get.off(Home());
      } else {
        Get.off(LoginScreen());
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            SizedBox(
              height: 20.0,
            ),
            Center(
              child: Image.asset(
                splashImagePath,
                fit: BoxFit.fill,
                height: 150.0,
                width: 150.0,
              ),
            )
          ],
        ),
      ),
    );
  }
}
