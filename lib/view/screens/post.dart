import 'dart:io';
import 'package:Unihub/view/screens/student_signup/student_signup_02.dart';
import 'package:Unihub/view/theme/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:multi_select_flutter/bottom_sheet/multi_select_bottom_sheet_field.dart';
import 'package:multi_select_flutter/chip_display/multi_select_chip_display.dart';
import 'package:multi_select_flutter/util/multi_select_item.dart';
import 'package:multi_select_flutter/util/multi_select_list_type.dart';

class Post extends StatefulWidget {
  @override
  _PostState createState() => _PostState();
}

class _PostState extends State<Post> {
  final _formKey = GlobalKey<FormState>();
  File _image;
  final picker = ImagePicker();
  static List<Interests> _interests = [
    Interests(id: 1, name: "Drawings"),
    Interests(id: 2, name: "Music"),
    Interests(id: 3, name: "Guitar"),
    Interests(id: 4, name: "Cricket"),
  ];

  List<Interests> _selectedAnimals2 = [];

  final _items = _interests
      .map((interest) => MultiSelectItem<Interests>(interest, interest.name))
      .toList();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('share_post'.tr),
        leading: IconButton(
          icon: Icon(Icons.close),
          onPressed: () => Get.back(),
        ),
      ),
      body: Form(
        key: _formKey,
        child: Column(
          children: [
            Expanded(
              child: ListView(
                children: [
                  Container(
                    padding: EdgeInsets.all(10.0),
                    child: TextFormField(
                      decoration: new InputDecoration(
                        hintText: "add_title".tr,
                      ),
                    ),
                  ),
                  Container(
                    padding:
                        EdgeInsets.only(left: 10.0, right: 10.0, bottom: 15.0),
                    child: TextFormField(
                      minLines: 6,
                      maxLines: 6,
                      keyboardType: TextInputType.multiline,
                      decoration: new InputDecoration(
                          hintText: "post_hint".tr,
                          focusedBorder:
                              _image != null ? InputBorder.none : null),
                    ),
                  ),
                  Visibility(
                    visible: _image != null,
                    child: Container(
                      padding: EdgeInsets.only(left: 15.0, right: 15.0),
                      child: Container(
                        // height: MediaQuery.of(context).size.height * 0.7,
                        alignment: Alignment.center,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          //content alignment to center
                          children: <Widget>[
                            Container(
                                //show image here after choosing image
                                child: _image == null
                                    ? Container()
                                    : //if uploadimage is null then show empty container
                                    Container(
                                        //elese show image here
                                        child: Image.file(_image),
                                      )),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 10.0, right: 10.0),
                    child: MultiSelectBottomSheetField(
                      initialChildSize: 0.4,
                      listType: MultiSelectListType.CHIP,
                      searchable: true,
                      buttonText: Text('add_category'.tr),
                      title: Text('add_category'.tr),
                      items: _items,
                      onConfirm: (values) {
                        _selectedAnimals2 = values;
                      },
                      chipDisplay: MultiSelectChipDisplay(
                        onTap: (value) {
                          setState(() {
                            _selectedAnimals2.remove(value);
                          });
                        },
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 15.0,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ElevatedButton.icon(
                        onPressed: () => getImage(),
                        style: ElevatedButton.styleFrom(
                            primary: primaryGreen,
                            minimumSize: Size(120.0, 36.0)),
                        icon: Icon(Icons.camera_alt_outlined),
                        label:
                            Text(_image == null ? 'add_photo'.tr : 'remove'.tr),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(
                top: 15.0,
                bottom: 24.0,
              ),
              child: ElevatedButton(
                child: Text('post_status'.tr),
                style: ElevatedButton.styleFrom(
                  minimumSize: Size(120.0, 36.0),
                  primary: primaryGreen,
                ),
                onPressed: () => _postStatus(),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _postStatus() {}

  Future getImage() async {
    if (_image != null) {
      setState(() {
        _image = null;
      });
    } else {
      final pickedFile = await picker.getImage(source: ImageSource.camera);

      setState(() {
        if (pickedFile != null) {
          _image = File(pickedFile.path);
        } else {
          print('No image selected.');
        }
      });
    }
  }
}
