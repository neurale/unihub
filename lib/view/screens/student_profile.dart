import 'dart:io';
import 'package:Unihub/controller/user_profile_controller.dart';
import 'package:Unihub/util/constants.dart';
import 'package:Unihub/view/theme/colors.dart';
import 'package:Unihub/view/widgets/custom/custom_widgets.dart';
import 'package:dash_chat/dash_chat.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:get_storage/get_storage.dart';
import 'package:image_picker/image_picker.dart';

class StudentProfile extends StatefulWidget {
  @override
  MapScreenState createState() => MapScreenState();
}

class MapScreenState extends State<StudentProfile> {
  File _image;
  final picker = ImagePicker();
  final GlobalKey<FormState> _formKeyValue = GlobalKey<FormState>();
  bool _isEditing = false;
  bool _isSummaryEditing = false;
  bool _isSocialEditing = false;
  bool _isInfoEditing = false;
  bool _isEduEditing = false;
  bool _isInterEditing = false;
  String _dob;
  TextEditingController _dobController;
  UserProfileController _controller;
  var user;
  final _storage = GetStorage();

  List<String> _higherEducationLevel = ["BSC", "MSC", "PHD", "Diploma", "HND"];
  List<String> _educationLevel = ["O/L", "A/L"];
  String _eduCategory;

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  @override
  void initState() {
    String token = _storage.read(accessTokenKey);
    _controller = Get.put(UserProfileController(token));
    if(_controller.profileData?.profile?.birthday != null){
      _dob = DateFormat('yyyy-MM-dd')
          .format(_controller.profileData?.profile?.birthday);
    }
    _dobController = TextEditingController(text: _dob);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    DateTime date = DateTime(1900);

    return Column(
      children: [
        Expanded(
          child: Form(
            key: _formKeyValue,
            child: Obx(() {
              if (_controller.isLoading.value) {
                return CustomWidget().getLoadingDualRing();
              } else {
                return ListView(
                  padding: EdgeInsets.only(left: 24.0, right: 24.0),
                  shrinkWrap: true,
                  children: [
                    Container(
                      padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
                      child: Center(
                        child: CircularProfileAvatar(
                          'https://www.pavilionweb.com/wp-content/uploads/2017/03/man-300x300.png',
                          child: _image != null
                              ? Image(
                                  image: FileImage(_image),
                                )
                              : null,
                          radius: 70,
                          backgroundColor: Colors.transparent,
                          borderWidth: 0.0,
                          borderColor: Colors.brown,
                          elevation: 5.0,
                          foregroundColor: Colors.brown.withOpacity(0.5),
                          cacheImage: true,
                          onTap: () => getImage(),
                          showInitialTextAbovePicture: true,
                        ),
                      ),
                    ),
                    Row(
                      children: [
                        Text('about_you'.tr),
                        Spacer(),
                        _getSummaryEditIcon(),
                      ],
                    ),
                    Text('tell_about_you'.tr),
                    TextFormField(
                      enabled: _isEditing && _isSummaryEditing,
                      autofocus: false,
                      obscureText: true,
                      decoration: InputDecoration(
                        floatingLabelBehavior: FloatingLabelBehavior.always,
                        labelText: 'summary'.tr,
                        hintText: 'summary'.tr,
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        // border: OutlineInputBorder(
                        //     borderRadius: BorderRadius.circular(32.0)),
                      ),
                    ),
                    SizedBox(height: 15.0),
                    Row(
                      children: [
                        Text('social_media_links'.tr),
                        Spacer(),
                        _getSocialEditIcon(),
                      ],
                    ),
                    Text('add_social_media_links'.tr),
                    SizedBox(height: 15.0),
                    TextFormField(
                      enabled: _isEditing && _isSocialEditing,
                      autofocus: false,
                      obscureText: true,
                      decoration: InputDecoration(
                        prefixIcon: Transform.scale(
                          scale: 0.7,
                          child: ImageIcon(
                            AssetImage("assets/images/linkedin.png"),
                            color: primaryBlack,
                          ),
                        ),
                        labelText: 'linkedin_link'.tr,
                        hintText: 'linkedin_link'.tr,
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        // border: OutlineInputBorder(
                        //     borderRadius: BorderRadius.circular(32.0)),
                      ),
                    ),
                    SizedBox(height: 8.0),
                    TextFormField(
                      enabled: _isEditing && _isSocialEditing,
                      autofocus: false,
                      obscureText: true,
                      decoration: InputDecoration(
                        prefixIcon: Transform.scale(
                          scale: 0.7,
                          child: ImageIcon(
                            AssetImage("assets/images/facebook.png"),
                            color: primaryBlack,
                          ),
                        ),
                        labelText: 'fb_link'.tr,
                        hintText: 'fb_link'.tr,
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        // border: OutlineInputBorder(
                        //     borderRadius: BorderRadius.circular(32.0)),
                      ),
                    ),
                    SizedBox(height: 8.0),
                    TextFormField(
                      enabled: _isEditing && _isSocialEditing,
                      autofocus: false,
                      obscureText: true,
                      decoration: InputDecoration(
                        prefixIcon: Transform.scale(
                          scale: 0.7,
                          child: ImageIcon(
                            AssetImage("assets/images/twitter.png"),
                            color: primaryBlack,
                          ),
                        ),
                        labelText: 'twitter_link'.tr,
                        hintText: 'twitter_link'.tr,
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        // border: OutlineInputBorder(
                        //     borderRadius: BorderRadius.circular(32.0)),
                      ),
                    ),
                    SizedBox(height: 8.0),
                    TextFormField(
                      enabled: _isEditing && _isSocialEditing,
                      autofocus: false,
                      obscureText: true,
                      decoration: InputDecoration(
                        prefixIcon: Transform.scale(
                          scale: 0.7,
                          child: ImageIcon(
                            AssetImage("assets/images/instagram.png"),
                            color: primaryBlack,
                          ),
                        ),
                        labelText: 'insta_link'.tr,
                        hintText: 'insta_link'.tr,
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        // border: OutlineInputBorder(
                        //     borderRadius: BorderRadius.circular(32.0)),
                      ),
                    ),
                    SizedBox(height: 15.0),
                    Row(
                      children: [
                        Text('edit_basic_info'.tr),
                        Spacer(),
                        _getEditInfoIcon(),
                      ],
                    ),
                    Text('test'),
                    SizedBox(height: 15.0),
                    TextFormField(
                      initialValue: _controller.profileData.profile.firstName,
                      enabled: _isEditing && _isInfoEditing,
                      keyboardType: TextInputType.name,
                      autofocus: false,
                      decoration: InputDecoration(
                        labelText: 'f_name'.tr,
                        hintText: 'f_name'.tr,
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        // border: OutlineInputBorder(
                        //     borderRadius: BorderRadius.circular(32.0)),
                      ),
                    ),
                    SizedBox(height: 8.0),
                    TextFormField(
                      initialValue: _controller.profileData.profile.lastName,
                      enabled: _isEditing && _isInfoEditing,
                      keyboardType: TextInputType.name,
                      autofocus: false,
                      decoration: InputDecoration(
                        labelText: 'l_name'.tr,
                        hintText: 'l_name'.tr,
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        // border: OutlineInputBorder(
                        //     borderRadius: BorderRadius.circular(32.0)),
                      ),
                    ),
                    SizedBox(height: 8.0),
                    TextFormField(
                      initialValue: _controller.profileData.profile.userName,
                      enabled: _isEditing && _isInfoEditing,
                      keyboardType: TextInputType.name,
                      autofocus: false,
                      decoration: InputDecoration(
                        labelText: 'u_name'.tr,
                        hintText: 'u_name'.tr,
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        // border: OutlineInputBorder(
                        //     borderRadius: BorderRadius.circular(32.0)),
                      ),
                    ),
                    SizedBox(height: 8.0),
                    TextFormField(
                      controller: _dobController,
                      enabled: _isEditing && _isInfoEditing,
                      onTap: () async {
                        FocusScope.of(context).requestFocus(new FocusNode());
                        date = await showDatePicker(
                            context: context,
                            initialDate: DateTime.now(),
                            firstDate: DateTime(1900),
                            lastDate: DateTime(2100));
                        _dobController.text =
                            DateFormat('yyyy-MM-dd').format(date);
                        setState(() {
                          _dob = _dobController.text;
                        });
                      },
                      keyboardType: TextInputType.datetime,
                      autofocus: false,
                      decoration: InputDecoration(
                        labelText: 'dob'.tr,
                        hintText: 'dob'.tr,
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                      ),
                    ),
                    SizedBox(height: 8.0),
                    TextFormField(
                      initialValue: _controller.profileData.email,
                      enabled: _isEditing && _isInfoEditing,
                      keyboardType: TextInputType.emailAddress,
                      autofocus: false,
                      decoration: InputDecoration(
                        labelText: 'email'.tr,
                        hintText: 'email'.tr,
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        // border: OutlineInputBorder(
                        //     borderRadius: BorderRadius.circular(32.0)),
                      ),
                    ),
                    SizedBox(height: 8.0),
                    TextFormField(
                      initialValue: _controller.profileData.profile.country,
                      enabled: _isEditing && _isInfoEditing,
                      keyboardType: TextInputType.name,
                      autofocus: false,
                      decoration: InputDecoration(
                        labelText: 'country'.tr,
                        hintText: 'country'.tr,
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                      ),
                    ),
                    SizedBox(height: 8.0),
                    TextFormField(
                      initialValue: _controller.profileData.profile.city,
                      enabled: _isEditing && _isInfoEditing,
                      keyboardType: TextInputType.name,
                      autofocus: false,
                      decoration: InputDecoration(
                        labelText: 'city'.tr,
                        hintText: 'city'.tr,
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                      ),
                    ),
                    SizedBox(height: 8.0),
                    TextFormField(
                      initialValue: _controller.profileData.profile.contactNo,
                      enabled: _isEditing && _isInfoEditing,
                      keyboardType: TextInputType.number,
                      autofocus: false,
                      decoration: InputDecoration(
                        labelText: 'contact_no'.tr,
                        hintText: 'contact_no'.tr,
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                      ),
                    ),
                    SizedBox(height: 15.0),
                    Row(
                      children: [
                        Text('current_edu'.tr),
                        Spacer(),
                        _getEditEduIcon(),
                      ],
                    ),
                    Text('current_edu_hint'.tr),
                    SizedBox(height: 15.0),
                    TextFormField(
                      initialValue:
                          _controller.profileData.profile.universityName,
                      enabled: _isEditing && _isEduEditing,
                      keyboardType: TextInputType.name,
                      autofocus: false,
                      decoration: InputDecoration(
                        labelText: 'university'.tr,
                        hintText: 'university'.tr,
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                      ),
                    ),
                    SizedBox(height: 8.0),
                    DropdownButtonFormField(
                      items: _higherEducationLevel.map((String level) {
                        return new DropdownMenuItem(
                            value: level,
                            child: Row(
                              children: <Widget>[
                                Text(level),
                              ],
                            ));
                      }).toList(),
                      onChanged: _isEduEditing && _isEditing
                          ? (newValue) {
                              setState(() => _eduCategory = newValue);
                            }
                          : null,
                      value: _eduCategory,
                      decoration: InputDecoration(
                        hintText: 'select'.tr,
                        labelText: 'edu_level'.tr,
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Text(
                          "add_more".tr,
                        ),
                      ),
                    ),
                    SizedBox(height: 8.0),
                    TextFormField(
                      initialValue: _controller.profileData.profile.schoolName,
                      enabled: _isEditing && _isEduEditing,
                      keyboardType: TextInputType.name,
                      autofocus: false,
                      decoration: InputDecoration(
                        labelText: 'school'.tr,
                        hintText: 'school'.tr,
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                      ),
                    ),
                    SizedBox(height: 8.0),
                    DropdownButtonFormField(
                      items: _educationLevel.map((String level) {
                        return new DropdownMenuItem(
                            value: level,
                            child: Row(
                              children: <Widget>[
                                Text(level),
                              ],
                            ));
                      }).toList(),
                      onChanged: _isEduEditing && _isEditing
                          ? (newValue) {
                              setState(() => _eduCategory = newValue);
                            }
                          : null,
                      value: _eduCategory,
                      decoration: InputDecoration(
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        hintText: 'select'.tr,
                        labelText: 'edu_level'.tr,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Text(
                          "add_more".tr,
                        ),
                      ),
                    ),
                    SizedBox(height: 15.0),
                    Row(
                      children: [
                        Text('area_of_interests'.tr),
                        Spacer(),
                        _getEditInterestsIcon(),
                      ],
                    ),
                    Text('area_of_interests_hint'.tr),
                    SizedBox(height: 15.0),
                    TextFormField(
                      initialValue: _controller.profileData.profile.city,
                      enabled: _isEditing && _isInterEditing,
                      keyboardType: TextInputType.name,
                      autofocus: false,
                      decoration: InputDecoration(
                        labelText: 'locations'.tr,
                        hintText: 'city'.tr,
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                      ),
                    ),
                    SizedBox(height: 8.0),
                    DropdownButtonFormField(
                      items: _higherEducationLevel.map((String level) {
                        return new DropdownMenuItem(
                            value: level,
                            child: Row(
                              children: <Widget>[
                                Text(level),
                              ],
                            ));
                      }).toList(),
                      onChanged: _isInterEditing && _isEditing
                          ? (newValue) {
                              setState(() => _eduCategory = newValue);
                            }
                          : null,
                      value: _eduCategory,
                      decoration: InputDecoration(
                        hintText: 'select'.tr,
                        labelText: 'city'.tr,
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Text(
                          "add_more".tr,
                        ),
                      ),
                    ),
                    TextFormField(
                      enabled: _isEditing && _isInterEditing,
                      keyboardType: TextInputType.name,
                      autofocus: false,
                      decoration: InputDecoration(
                        labelText: 'categories'.tr,
                        hintText: 'subjects'.tr,
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                      ),
                    ),
                    SizedBox(height: 8.0),
                    DropdownButtonFormField(
                      items: _higherEducationLevel.map((String level) {
                        return new DropdownMenuItem(
                            value: level,
                            child: Row(
                              children: <Widget>[
                                Text(level),
                              ],
                            ));
                      }).toList(),
                      onChanged: _isInterEditing && _isEditing
                          ? (newValue) {
                              setState(() => _eduCategory = newValue);
                            }
                          : null,
                      value: _eduCategory,
                      decoration: InputDecoration(
                        hintText: 'select'.tr,
                        labelText: 'categories'.tr,
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Text(
                          "add_more".tr,
                        ),
                      ),
                    ),
                  ],
                );
              }
            }),
          ),
        ),
        Visibility(
          visible: _isEditing,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                padding: EdgeInsets.only(right: 5.0),
                child: ElevatedButton(
                  onPressed: () => setState(() {
                    resetForm();
                  }),
                  style:
                      ElevatedButton.styleFrom(minimumSize: Size(120.0, 36.0)),
                  child:
                      Text('cancel'.tr, style: TextStyle(color: Colors.white)),
                ),
              ),
              ElevatedButton(
                onPressed: () => setState(() {
                  resetForm();
                }),
                style: ElevatedButton.styleFrom(minimumSize: Size(120.0, 36.0)),
                child: Text('save_changes'.tr,
                    style: TextStyle(color: Colors.white)),
              ),
            ],
          ),
        ),
      ],
    );
  }

  resetForm() {
    _isEditing = false;
    _isSocialEditing = false;
    _isSummaryEditing = false;
    _isEduEditing = false;
    _isInterEditing = false;
  }

  Widget _getEditInfoIcon() {
    return new GestureDetector(
      child: CustomWidget().getEditPencil(),
      onTap: () {
        setState(() {
          _isEditing = true;
          _isInfoEditing = true;
        });
      },
    );
  }

  Widget _getEditEduIcon() {
    return new GestureDetector(
      child: CustomWidget().getEditPencil(),
      onTap: () {
        setState(() {
          _isEditing = true;
          _isEduEditing = true;
        });
      },
    );
  }

  Widget _getEditInterestsIcon() {
    return new GestureDetector(
      child: CustomWidget().getEditPencil(),
      onTap: () {
        setState(() {
          _isEditing = true;
          _isInterEditing = true;
        });
      },
    );
  }

  Widget _getSocialEditIcon() {
    return new GestureDetector(
      child: CustomWidget().getEditPencil(),
      onTap: () {
        setState(() {
          _isEditing = true;
          _isSocialEditing = true;
        });
      },
    );
  }

  Widget _getSummaryEditIcon() {
    return new GestureDetector(
      child: CustomWidget().getEditPencil(),
      onTap: () {
        setState(() {
          _isEditing = true;
          _isSummaryEditing = true;
        });
      },
    );
  }
}
