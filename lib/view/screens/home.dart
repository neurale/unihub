import 'package:Unihub/services/logout_service.dart';
import 'package:Unihub/util/common.dart';
import 'package:Unihub/util/constants.dart';
import 'package:Unihub/view/screens/contacts.dart';
import 'package:Unihub/view/screens/feed.dart';
import 'package:Unihub/view/screens/post.dart';
import 'package:Unihub/view/screens/search/search.dart';
import 'package:Unihub/view/screens/student_profile.dart';
import 'package:Unihub/view/screens/student_signup/student_signup_01.dart';
import 'package:Unihub/view/screens/notification/notifications.dart';
import 'package:Unihub/view/theme/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'institute/institute_list.dart';
import 'package:get_storage/get_storage.dart';
import 'package:Unihub/services/refresh_token_service.dart';

import 'login.dart';

class Home extends StatefulWidget {
  const Home();

  @override
  _HomeState createState() => new _HomeState();
}

class _HomeState extends State<Home> with SingleTickerProviderStateMixin {
  static final GlobalKey<ScaffoldState> scaffoldKey =
      new GlobalKey<ScaffoldState>();
  int _currentIndex = 0;
  bool _hasNotifications = false;
  final _storage = GetStorage();

  Widget pageCaller(int index) {
    switch (index) {
      case 0:
        {
          return Feed();
        }
        break;
      case 1:
        {
          return Contacts();
        }
        break;
      case 3:
        {
          return Notifications();
        }
        break;
      case 4:
        {
          return StudentProfile();
        }
        break;
      default:
        {
          return Container();
        }
    }
  }

  void _onTabTapped(int index) {
    if (index != 2) {
      setState(() {
        _currentIndex = index;
      });
    } else {
      Get.to(Post());
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: scaffoldKey,
      appBar: new AppBar(
        actions: [
          IconButton(
            icon: const Icon(Icons.search),
            onPressed: () => Get.to(SearchPage()),
          ),
        ],
      ),
      drawer: Container(
        width: 220.0,
        height: 620,
        child: ClipRRect(
          borderRadius: BorderRadius.vertical(
              top: Radius.circular(10.0), bottom: Radius.circular(10.0)),
          child: Drawer(
            child: ListView(
              // Important: Remove any padding from the ListView.
              padding: EdgeInsets.zero,
              children: <Widget>[
                UserAccountsDrawerHeader(
                  accountName: Text(
                    "John Doe",
                    style: TextStyle(color: primaryGreen),
                  ),
                  currentAccountPicture: CircleAvatar(
                      radius: 50,
                      backgroundImage: AssetImage('assets/images/pacman.png')),
                  decoration: BoxDecoration(),
                  accountEmail: Text(
                    'admin@unihub.com',
                    style: TextStyle(color: primaryBlack),
                  ),
                ),
                ListTile(
                  title: Text(
                    'about_us'.tr,
                    style: TextStyle(color: primaryGreen),
                  ),
                  leading: Icon(
                    Icons.info,
                    color: primaryGreen,
                  ),
                  onTap: () {},
                ),
                ListTile(
                  title: Text(
                    'settings'.tr,
                    style: TextStyle(color: primaryGreen),
                  ),
                  leading: Icon(
                    Icons.settings,
                    color: primaryGreen,
                  ),
                  onTap: () => Get.to(InstituteList(
                    type: UniListItemType.university,
                  )),
                ),
                ListTile(
                  title: Text(
                    'log_out'.tr,
                    style: TextStyle(color: primaryGreen),
                  ),
                  leading: Icon(
                    Icons.exit_to_app,
                    color: primaryGreen,
                  ),
                  onTap: () async {
                    String token = _storage.read(accessTokenKey);
                    try {
                      LogoutService service = Get.put(LogoutService());
                      RefreshTokenService refreshService = Get.put(RefreshTokenService());
                      dynamic refreshedToken = await refreshService.fetchRefreshToken(token);
                      refreshedToken = refreshedToken.body['access_token'];
                      var response = await service.fetchLogoutInfo(refreshedToken);
                      if (response.status.isOk) {
                        showInfoToast(response.body['message']);
                        _storage.remove(accessTokenKey);
                        Get.off(LoginScreen());
                      } else {
                        showErrorToast(response.statusText);
                      }
                    } finally {
                      print('logged out');
                    }
                  },
                ),
                ListTile(
                  title: Text(
                    'Signup',
                    style: TextStyle(color: primaryGreen),
                  ),
                  leading: Icon(
                    Icons.exit_to_app,
                    color: primaryGreen,
                  ),
                  onTap: () => {Get.to(StudentSignup01())},
                ),
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        selectedLabelStyle: TextStyle(fontSize: 13.0),
        onTap: _onTabTapped,
        type: BottomNavigationBarType.fixed,
        currentIndex: _currentIndex,
        // this will be set when a new tab is tapped
        items: [
          BottomNavigationBarItem(
            icon: new Icon(Icons.home),
            label: 'home'.tr,
          ),
          BottomNavigationBarItem(
            icon: new Icon(Icons.mail),
            label: 'messages'.tr,
          ),
          BottomNavigationBarItem(
            icon: new Icon(Icons.post_add),
            label: 'post'.tr,
          ),
          BottomNavigationBarItem(
            icon: _hasNotifications
                ? new Icon(Icons.notifications)
                : new Icon(Icons.notifications_none),
            label: 'notifications'.tr,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: 'profile'.tr,
          ),
        ],
      ),
      body: pageCaller(_currentIndex),
    );
  }
}
