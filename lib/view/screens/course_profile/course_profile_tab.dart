import 'package:Unihub/util/constants.dart';
import 'package:Unihub/view/screens/institute/institute_list.dart';
import 'package:Unihub/view/screens/university_profile/university_profile_review.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:Unihub/view/screens/course_profile/course_profile_about.dart';

class CourseProfileTab extends StatefulWidget {
  final int courseId;

  const CourseProfileTab({Key key, this.courseId}) : super(key: key);

  @override
  _CourseProfileTabState createState() => _CourseProfileTabState();
}

class _CourseProfileTabState extends State<CourseProfileTab> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            bottom: PreferredSize(
                child: TabBar(
                    isScrollable: true,
                    unselectedLabelColor: Colors.white.withOpacity(0.3),
                    indicatorColor: Colors.white,
                    tabs: [
                      Tab(
                        child: Text('about'.tr),
                      ),
                      Tab(
                        child: Text('subjects'.tr),
                      ),
                      Tab(
                        child: Text('reviews'.tr),
                      ),
                    ]),
                preferredSize: Size.fromHeight(30.0)),
          ),
          body: TabBarView(
            children: <Widget>[
              CourseProfileAbout(
                courseId: widget.courseId,
              ),
              InstituteList(
                type: UniListItemType.subject,
              ),
              UniversityReview(
                writeReview: true,
              ),
            ],
          )),
    );
  }
}
