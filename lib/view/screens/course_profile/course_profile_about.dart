import 'dart:io';

import 'package:Unihub/view/theme/styles.dart';
import 'package:Unihub/view/widgets/custom/custom_widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:Unihub/view/screens/institute/institute_profile.dart';
import 'package:get/get.dart';
import 'package:Unihub/controller/course_info_controller.dart';
import 'package:Unihub/util/common.dart';

class CourseProfileAbout extends StatefulWidget {
  final int courseId;

  const CourseProfileAbout({Key key, this.courseId}) : super(key: key);

  @override
  _CourseProfileAboutState createState() => _CourseProfileAboutState();
}

class _CourseProfileAboutState extends State<CourseProfileAbout> {
  final GlobalKey<FormState> _formKeyValue = GlobalKey<FormState>();
  final picker = ImagePicker();
  File _image;
  final _customWidget = CustomWidget();
  CourseInfoController _controller;

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  @override
  void initState() {
    _controller = Get.put(CourseInfoController(widget.courseId.toString()));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: _formKeyValue,
        child: Obx(() {
          if (_controller.isLoading.value) {
            return CustomWidget().getLoadingDualRing();
          } else {
            return ListView(
              padding: EdgeInsets.only(left: 24.0, right: 24.0, bottom: 24.0),
              shrinkWrap: true,
              children: [
                Column(
                  children: [
                    InstituteProfile().getInstituteProfile(
                        institute: _controller.courseInfo.courseName,
                        description: _controller.courseInfo.tagline,
                        rating: 4,
                        followers: 5,
                        about: _controller.courseInfo.summary),
                    Text(
                      'overview'.tr,
                      style: subtitleTextStyle,
                    ),
                    SizedBox(
                      height: 15.0,
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: 120.0,
                          child: Text(
                            'parent_dep'.tr,
                            style: boldFontStyle,
                          ),
                        ),
                        _customWidget.getMultiLineTextField(
                            _controller.courseInfo.department.departmentName),
                      ],
                    ),
                    SizedBox(
                      height: 8.0,
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: 120.0,
                          child: Text(
                            'parent_college'.tr,
                            style: boldFontStyle,
                          ),
                        ),
                        _customWidget.getMultiLineTextField(
                            _controller.courseInfo.collegeId.toString()),
                      ],
                    ),
                    SizedBox(
                      height: 8.0,
                    ),
                    /*Row(
                      children: [
                        SizedBox(
                          width: 120.0,
                          child: Text(
                            'available_campuses'.tr,
                            style: boldFontStyle,
                          ),
                        ),
                        _customWidget.getMultiLineTextField('',
                            list: getCampusNames(
                                _controller.courseInfo.campuses)),
                      ],
                    ),*/
                    SizedBox(
                      height: 8.0,
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: 120.0,
                          child: Text(
                            'course_fee'.tr,
                            style: boldFontStyle,
                          ),
                        ),
                        _customWidget.getMultiLineTextField(
                            _controller.courseInfo.courseFee),
                      ],
                    ),
                    SizedBox(
                      height: 8.0,
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: 120.0,
                          child: Text(
                            'career_options'.tr,
                            style: boldFontStyle,
                          ),
                        ),
                        _customWidget.getMultiLineTextField('',
                            list: _controller.courseInfo.careers),
                      ],
                    ),
                    SizedBox(
                      height: 8.0,
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: 120.0,
                          child: Text(
                            'more_details'.tr,
                            style: boldFontStyle,
                          ),
                        ),
                        _customWidget.getMultiLineTextField(
                            _controller.courseInfo.summary),
                      ],
                    ),
                    SizedBox(
                      height: 8.0,
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: 120.0,
                          child: Text(
                            'app_link'.tr,
                            style: boldFontStyle,
                          ),
                        ),
                        _customWidget.getMultiLineTextField(
                            _controller.courseInfo.local),
                      ],
                    ),
                  ],
                ),
              ],
            );
          }
        }),
      ),
    );
  }
}
