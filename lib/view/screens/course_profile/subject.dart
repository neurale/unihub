import 'package:Unihub/controller/subject_info_controller.dart';
import 'package:Unihub/view/theme/styles.dart';
import 'package:Unihub/view/widgets/custom/custom_widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:Unihub/view/screens/institute/institute_profile.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';

class Subject extends StatefulWidget {
  final int id;

  const Subject({Key key, this.id}) : super(key: key);

  @override
  _SubjectState createState() => _SubjectState();
}

class _SubjectState extends State<Subject> {
  final GlobalKey<FormState> _formKeyValue = GlobalKey<FormState>();
  SubjectInfoController _controller;

  @override
  void initState() {
    _controller = Get.put(SubjectInfoController(widget.id.toString()));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Form(
        key: _formKeyValue,
        child: Obx(() {
          if (_controller.isLoading.value) {
            return CustomWidget().getLoadingDualRing();
          } else {
            return ListView(
              padding: EdgeInsets.only(left: 24.0, right: 24.0, bottom: 24.0),
              shrinkWrap: true,
              children: [
                Column(
                  children: [
                    InstituteProfile().getInstituteProfile(
                      institute: _controller.subjectInfo.subjectName,
                      description: _controller.subjectInfo.tagline,
                      about: _controller.subjectInfo.summary,
                    ),
                    Text(
                      'about'.tr,
                      style: subtitleTextStyle,
                    ),
                    SizedBox(
                      height: 15.0,
                    ),
                    Text(_controller.subjectInfo.summary),
                    SizedBox(
                      height: 15.0,
                    ),
                    Text(
                      'additional_info'.tr,
                      style: subtitleTextStyle,
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: 120.0,
                          child: Text(
                            'syllabus'.tr,
                            style: boldFontStyle,
                          ),
                        ),
                        Text(_controller.subjectInfo.syllabus),
                      ],
                    ),
                    SizedBox(
                      height: 8.0,
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: 120.0,
                          child: Text(
                            'credit_points'.tr,
                            style: boldFontStyle,
                          ),
                        ),
                        Text(_controller.subjectInfo.creditPoints ?? '0'),
                      ],
                    ),
                  ],
                ),
              ],
            );
          }
        }),
      ),
    );
  }
}
