import 'package:Unihub/view/theme/styles.dart';
import 'package:Unihub/view/widgets/custom/custom_widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:grouped_list/grouped_list.dart';
import 'package:get/get.dart';

class Notifications extends StatefulWidget {
  const Notifications({Key key}) : super(key: key);

  @override
  _NotificationsState createState() => _NotificationsState();
}

class _NotificationsState extends State<Notifications> {
  List _elements = [
    {
      'name':
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sit amet viverra justo. Sed ut hendrerit nunc, elementum laoreet felis. Suspendisse molestie porta scelerisque. Suspendisse egestas, odio sed scelerisque accumsan, dui massa dapibus nulla, non pulvinar enim dolor interdum urna. Nunc sed laoreet arcu, ac maximus odio. Pellentesque condimentum ipsum at faucibus pretium. Maecenas imperdiet gravida est a sodales. Nulla commodo',
      'group': 'Today'
    },
    {'name': 'Will', 'group': 'Today'},
    {'name': 'Beth', 'group': 'Today'},
    {'name': 'Miranda', 'group': 'Earlier'},
    {'name': 'Mike', 'group': 'Earlier'},
    {'name': 'Danny', 'group': 'Earlier'},
    {'name': 'Danny', 'group': 'Earlier'},
    {'name': 'Danny', 'group': 'Earlier'},
    {'name': 'Danny', 'group': 'Earlier'},
    {'name': 'Danny', 'group': 'Earlier'},
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      child: GroupedListView<dynamic, String>(
        elements: _elements,
        groupBy: (element) => element['group'],
        groupComparator: (value1, value2) => value2.compareTo(value1),
        itemComparator: (item1, item2) =>
            item1['name'].compareTo(item2['name']),
        order: GroupedListOrder.ASC,
        useStickyGroupSeparators: true,
        groupSeparatorBuilder: (String value) => Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            value,
            textAlign: TextAlign.center,
            style: titleTextStyle,
          ),
        ),
        itemBuilder: (c, element) {
          return Card(
            elevation: 2.0,
            margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
            child: Container(
              child: ListTile(
                contentPadding:
                    EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                title: Text(
                  element['name'],
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                ),
                subtitle: Text('2 hours ago'),
                trailing: CustomWidget().getCustomOutlinedButton(
                    buttonText: 'view_post'.tr, onPressed: () {}),
              ),
            ),
          );
        },
      ),
    );
  }
}
