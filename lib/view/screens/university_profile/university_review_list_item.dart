import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:Unihub/view/theme/styles.dart';

class UniversityReviewListItem extends StatelessWidget {
  final String userName, subtitle, comment, age;
  final double rating;

  const UniversityReviewListItem(
      {Key key,
      this.userName,
      this.comment,
      this.age,
      this.rating,
      this.subtitle})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ListTile(
            leading: Visibility(
              child: ImageIcon(
                AssetImage("assets/images/twitter.png"),
              ),
            ),
            title: Row(
              children: [
                Text(
                  userName ?? '',
                  style: boldFontStyle,
                ),
                Spacer(),
                Icon(Icons.star),
                Text(rating.toString()),
              ],
            ),
            subtitle: Row(
              children: [
                Text(subtitle ?? ''),
                Spacer(),
                Text(age ?? ''),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15.0, right: 15.0),
            child: Text(
              comment ?? '',
              textAlign: TextAlign.justify,
              maxLines: 3,
            ),
          ),
          SizedBox(width: 15.0),
        ],
      ),
    );
    ;
  }
}
