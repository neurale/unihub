import 'dart:io';
import 'package:Unihub/view/theme/colors.dart';
import 'package:Unihub/view/widgets/custom/custom_widgets.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:get/get.dart';
import 'package:Unihub/view/theme/styles.dart';
import 'package:Unihub/view/screens/institute/institute_profile.dart';
import 'package:Unihub/controller/university_info_controller.dart';

import 'image_gallery.dart';

class UniversityProfileAbout extends StatefulWidget {
  final int uniId;

  const UniversityProfileAbout({Key key, this.uniId}) : super(key: key);

  @override
  _UniversityProfileAboutState createState() => _UniversityProfileAboutState();
}

class _UniversityProfileAboutState extends State<UniversityProfileAbout> {
  final GlobalKey<FormState> _formKeyValue = GlobalKey<FormState>();
  final picker = ImagePicker();
  File _image;
  final _customWidget = CustomWidget();
  UniversityInfoController _controller;

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  @override
  void initState() {
    _controller = Get.put(UniversityInfoController(widget.uniId.toString()));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
          key: _formKeyValue,
          child: Obx(() {
            if (_controller.isLoading.value) {
              return CustomWidget().getLoadingDualRing();
            } else {
              return ListView(
                padding: EdgeInsets.only(left: 24.0, right: 24.0, bottom: 24.0),
                shrinkWrap: true,
                children: [
                  Column(
                    children: [
                      InstituteProfile().getInstituteProfile(
                        institute: _controller?.universityInfo?.universityName ?? '',
                        description: _controller.universityInfo.tagline,
                        about: _controller.universityInfo.summary,
                        followers: 10,
                        rating: 3,
                      ),
                      Text(
                        'overview'.tr,
                        style: subtitleTextStyle,
                      ),
                      SizedBox(
                        height: 15.0,
                      ),
                      Row(
                        children: [
                          SizedBox(
                            width: 120.0,
                            child: Text(
                              'website'.tr,
                              style: boldFontStyle,
                            ),
                          ),
                          _customWidget.getMultiLineTextField(
                              _controller.universityInfo.website),
                        ],
                      ),
                      SizedBox(
                        height: 8.0,
                      ),
                      Row(
                        children: [
                          SizedBox(
                            width: 120.0,
                            child: Text(
                              'world_rank'.tr,
                              style: boldFontStyle,
                            ),
                          ),
                          _customWidget.getMultiLineTextField(
                              _controller.universityInfo.worldRank),
                        ],
                      ),
                      SizedBox(
                        height: 8.0,
                      ),
                      Row(
                        children: [
                          SizedBox(
                            width: 120.0,
                            child: Text(
                              'country_rank'.tr,
                              style: boldFontStyle,
                            ),
                          ),
                          _customWidget.getMultiLineTextField(
                              _controller.universityInfo.countryRank),
                        ],
                      ),
                      SizedBox(
                        height: 8.0,
                      ),
                      Row(
                        children: [
                          SizedBox(
                            width: 120.0,
                            child: Text(
                              'established_yr'.tr,
                              style: boldFontStyle,
                            ),
                          ),
                          _customWidget.getMultiLineTextField(_controller
                              .universityInfo?.establishedOn?.timeZoneName ?? ''),
                        ],
                      ),
                      SizedBox(
                        height: 8.0,
                      ),
                      Row(
                        children: [
                          SizedBox(
                            width: 120.0,
                            child: Text(
                              'specialities'.tr,
                              style: boldFontStyle,
                            ),
                          ),
                          _customWidget.getMultiLineTextField(
                              _controller.universityInfo.speciality),
                        ],
                      ),
                      SizedBox(
                        height: 8.0,
                      ),
                      Row(
                        children: [
                          SizedBox(
                            width: 120.0,
                            child: Text(
                              'hq'.tr,
                              style: boldFontStyle,
                            ),
                          ),
                          _customWidget.getMultiLineTextField(
                              _controller.universityInfo.country),
                        ],
                      ),
                      SizedBox(
                        height: 8.0,
                      ),
                      Row(
                        children: [
                          SizedBox(
                            width: 120.0,
                            child: Text(
                              'contact_info'.tr,
                              style: boldFontStyle,
                            ),
                          ),
                          _customWidget.getMultiLineTextField('',
                              list: _controller.universityInfo.contactNo),
                        ],
                      ),
                      SizedBox(
                        height: 8.0,
                      ),
                      Row(
                        children: [
                          SizedBox(
                            width: 120.0,
                            child: Text(
                              'location'.tr,
                              style: boldFontStyle,
                            ),
                          ),
                          _customWidget.getMultiLineTextField(
                              _controller.universityInfo.location),
                        ],
                      ),
                      SizedBox(
                        height: 15.0,
                      ),
                      Text(
                        'additional_info'.tr,
                        style: subtitleTextStyle,
                      ),
                      SizedBox(
                        height: 15.0,
                      ),
                      Row(
                        children: [
                          SizedBox(
                            width: 120.0,
                            child: Text(
                              'type'.tr,
                              style: boldFontStyle,
                            ),
                          ),
                          _customWidget.getMultiLineTextField(
                              _controller.universityInfo.speciality),
                        ],
                      ),
                      SizedBox(
                        height: 8.0,
                      ),
                      Row(
                        children: [
                          SizedBox(
                            width: 120.0,
                            child: Text(
                              'fac_staff_count'.tr,
                              style: boldFontStyle,
                            ),
                          ),
                          _customWidget.getMultiLineTextField(
                              _controller.universityInfo.staff.toString()),
                        ],
                      ),
                      SizedBox(
                        height: 8.0,
                      ),
                      Row(
                        children: [
                          SizedBox(
                            width: 120.0,
                            child: Text(
                              'student_count'.tr,
                              style: boldFontStyle,
                            ),
                          ),
                          _customWidget.getMultiLineTextField(_controller
                              .universityInfo.locStudents
                              .toString()),
                        ],
                      ),
                      SizedBox(
                        height: 8.0,
                      ),
                      Row(
                        children: [
                          SizedBox(
                            width: 120.0,
                            child: Text(
                              'inter_std_count'.tr,
                              style: boldFontStyle,
                            ),
                          ),
                          _customWidget.getMultiLineTextField(_controller
                              .universityInfo.intStudents
                              .toString()),
                        ],
                      ),
                      SizedBox(
                        height: 8.0,
                      ),
                      Row(
                        children: [
                          SizedBox(
                            width: 120.0,
                            child: Text(
                              'pictures'.tr,
                              style: boldFontStyle,
                            ),
                          ),
                          Column(
                            children: [
                              Row(
                                children: [
                                  CachedNetworkImage(
                                    fit: BoxFit.fill,
                                    height: 100.0,
                                    width: 100.0,
                                    imageUrl:
                                        "https://placeimg.com/500/500/any",
                                    placeholder: (context, url) =>
                                        CircularProgressIndicator(),
                                    errorWidget: (context, url, error) =>
                                        Icon(Icons.error),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 4.0, bottom: 4.0),
                                    child: CachedNetworkImage(
                                      height: 100.0,
                                      width: 100.0,
                                      imageUrl:
                                          "https://placeimg.com/500/500/any",
                                      placeholder: (context, url) =>
                                          CircularProgressIndicator(),
                                      errorWidget: (context, url, error) =>
                                          Icon(Icons.error),
                                    ),
                                  ),
                                ],
                              ),
                              CachedNetworkImage(
                                fit: BoxFit.fill,
                                height: 100.0,
                                width: 204.0,
                                imageUrl: "https://placeimg.com/500/500/any",
                                placeholder: (context, url) =>
                                    CircularProgressIndicator(),
                                errorWidget: (context, url, error) =>
                                    Icon(Icons.error),
                              ),
                              _customWidget.getCustomOutlinedButton(
                                  buttonText: "see_more".tr,
                                  onPressed: () => Get.to(Gallery())),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              );
            }
          })),
    );
  }
}
