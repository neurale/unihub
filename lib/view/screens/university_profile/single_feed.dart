import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class SingleFeed extends StatefulWidget {
  static String tag = 'login-page';

  @override
  _SingleFeedState createState() => new _SingleFeedState();
}

class _SingleFeedState extends State<SingleFeed> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(),
        body: SafeArea(
          child: new Container(
            color: Colors.white,
            child: new ListView(
              children: <Widget>[
                _feedCard(context),
                TextFormField(
                  maxLines: 3,
                  cursorColor: Colors.black,
                  // keyboardType: inputType,
                  decoration: new InputDecoration(
                      suffixIcon: Icon(Icons.send),
                      // prefix: CircleAvatar(
                      //   radius: 20,
                      //   backgroundImage: NetworkImage(
                      //       'https://cdn4.iconfinder.com/data/icons/avatars-xmas-giveaway/128/girl_female_woman_avatar-512.png'),
                      // ),
                      border: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      errorBorder: InputBorder.none,
                      disabledBorder: InputBorder.none,
                      contentPadding: EdgeInsets.only(
                          left: 15, bottom: 11, top: 11, right: 15),
                      hintText: "Leave your comment here"),
                )
              ],
            ),
          ),
        ));
  }
}

Widget _commentCard(BuildContext context, String type) {
  if (type != 'reply') {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        children: [
          Row(
            children: [
              CircleAvatar(
                backgroundImage: NetworkImage(
                    'https://cdn4.iconfinder.com/data/icons/avatars-xmas-giveaway/128/girl_female_woman_avatar-512.png'),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('Nimali Wasana'),
              ),
              Text('2 m ago')
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(left: 50),
            child: Text(
              'Greyhound divisively hello coldly wonderfully marginally far upon excluding.Greyhound divisively hello coldly wonderfully marginally far upon excluding.',
              style: TextStyle(color: Colors.black.withOpacity(0.6)),
            ),
          )
        ],
      ),
    );
  } else {
    return Padding(
      padding: const EdgeInsets.only(left: 60),
      child: Column(
        children: [
          Row(
            children: [
              CircleAvatar(
                backgroundImage: NetworkImage(
                    'https://cdn4.iconfinder.com/data/icons/avatars-xmas-giveaway/128/girl_female_woman_avatar-512.png'),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('Nimali Wasana'),
              ),
              Text('2 m ago')
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(left: 50),
            child: Text(
              'Greyhound divisively hello coldly wonderfully marginally far upon excluding.Greyhound divisively hello coldly wonderfully marginally far upon excluding.',
              style: TextStyle(color: Colors.black.withOpacity(0.6)),
            ),
          )
        ],
      ),
    );
  }
}

Widget _feedCard(BuildContext context) {
  return Padding(
    padding: const EdgeInsets.only(top: 3),
    child: Card(
      clipBehavior: Clip.antiAlias,
      elevation: 2.0,
      child: Column(
        children: [
          ListTile(
            leading: CircleAvatar(
              backgroundImage: NetworkImage(
                  'https://library.kissclipart.com/20180918/hyw/kissclipart-universities-icons-clipart-university-clip-art-f9ea22390606b78a.jpg'),
            ),
            title: const Text('UCSC'),
            subtitle: Text(
              'The best',
              style: TextStyle(color: Colors.black.withOpacity(0.6)),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(
              'Greyhound divisively hello coldly wonderfully marginally far upon excluding.',
              style: TextStyle(color: Colors.black.withOpacity(0.6)),
            ),
          ),
          Image.network(
              'https://images.pexels.com/photos/1563356/pexels-photo-1563356.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500'),
          ButtonBar(
            alignment: MainAxisAlignment.start,
            children: [
              // FlatButton(
              //   onPressed: () {
              //   },
              //   child: const Text('Like'),
              // ),
              // FlatButton(
              //   onPressed: () {
              //   },
              //   child: const Text('Comment'),
              // ),
              // FlatButton(
              //   onPressed: () {
              //   },
              //   child: const Text('Share'),
              // ),
            ],
          ),
          Align(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text('Comments'),
            ),
            alignment: Alignment.centerLeft,
          ),
          _commentCard(context, 'comment'),
          _commentCard(context, 'reply'),
          _commentCard(context, 'comment'),
          _commentCard(context, 'reply'),
          _commentCard(context, 'comment'),
        ],
      ),
    ),
  );
}
