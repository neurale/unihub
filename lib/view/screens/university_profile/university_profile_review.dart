import 'package:Unihub/view/screens/university_profile/university_review_list_item.dart';
import 'package:Unihub/view/theme/colors.dart';
import 'package:Unihub/view/theme/styles.dart';
import 'package:Unihub/view/widgets/custom/custom_widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class UniversityReview extends StatefulWidget {
  final bool writeReview;

  const UniversityReview({Key key, this.writeReview}) : super(key: key);

  @override
  _UniversityReviewState createState() => _UniversityReviewState();
}

class _UniversityReviewState extends State<UniversityReview> {
  int reviewsCount = 0;
  double overallRating = 0.0;
  int _listItemLimit = 5;
  List<UniversityReviewListItem> _reviewList = [
    UniversityReviewListItem(),
    UniversityReviewListItem(),
    UniversityReviewListItem(),
    UniversityReviewListItem(),
    UniversityReviewListItem(),
    UniversityReviewListItem(),
  ];

  @override
  Widget build(BuildContext context) {
    if (_reviewList.length < _listItemLimit) {
      setState(() {
        _listItemLimit = _reviewList.length;
      });
    }

    return Container(
      padding:
          EdgeInsets.only(left: 24.0, right: 24.0, bottom: 24.0, top: 24.0),
      child: Column(
        children: [
          Row(
            children: [
              Text(
                reviewsCount.toString() + ' ' + 'reviews'.tr,
                style: titleTextStyle,
              ),
              Spacer(),
              RichText(
                text: TextSpan(
                  text: 'see_all'.tr,
                  recognizer: TapGestureRecognizer()
                    ..onTap = () {
                      setState(() {
                        _listItemLimit = _reviewList.length;
                      });
                    },
                  style: TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                ),
              )
            ],
          ),
          SizedBox(
            height: 10.0,
          ),
          Row(
            children: [
              Text(
                'overall_rating'.tr,
                style: boldFontStyle,
              ),
              Spacer(),
              Icon(Icons.star),
              Text(overallRating.toString()),
            ],
          ),
          SizedBox(
            height: 8.0,
          ),
          Divider(),
          SizedBox(
            height: 8.0,
          ),
          Expanded(
            child: _reviewList == null
                ? CustomWidget().getLoadingDualRing()
                : ListView.builder(
                    itemCount: _listItemLimit,
                    itemBuilder: (context, index) {
                      return UniversityReviewListItem(
                        age: '3 Months ago',
                        comment: 'This is a sample comment',
                        rating: 3,
                        userName: 'Hiran Lakshika',
                        subtitle: 'Student',
                      );
                    }),
          ),
          Visibility(
            visible: widget.writeReview ?? false,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: primaryGreen,
              ),
              onPressed: () {},
              child: Text('write_review'.tr),
            ),
          ),
        ],
      ),
    );
  }
}
