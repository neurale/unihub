import 'package:Unihub/util/constants.dart';
import 'package:Unihub/view/screens/university_profile/feed.dart';
import 'package:Unihub/view/screens/university_profile/university_profile_about.dart';
import 'package:Unihub/view/screens/institute//institute_list.dart';
import 'package:Unihub/view/screens/university_profile/university_profile_review.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class UniversityProfileTab extends StatefulWidget {
  final int uniId;

  const UniversityProfileTab({Key key, this.uniId}) : super(key: key);

  @override
  _UniversityProfileTabState createState() => _UniversityProfileTabState();
}

class _UniversityProfileTabState extends State<UniversityProfileTab> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 7,
      child: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            bottom: PreferredSize(
                child: TabBar(
                    isScrollable: true,
                    unselectedLabelColor: Colors.white.withOpacity(0.3),
                    indicatorColor: Colors.white,
                    tabs: [
                      Tab(
                        child: Text('about'.tr),
                      ),
                      Tab(
                        child: Text('updates'.tr),
                      ),
                      Tab(
                        child: Text('campuses'.tr),
                      ),
                      Tab(
                        child: Text('colleges'.tr),
                      ),
                      Tab(
                        child: Text('departments'.tr),
                      ),
                      Tab(
                        child: Text('courses'.tr),
                      ),
                      Tab(
                        child: Text('reviews'.tr),
                      ),
                    ]),
                preferredSize: Size.fromHeight(30.0)),
          ),
          body: TabBarView(
            children: <Widget>[
              UniversityProfileAbout(
                uniId: widget.uniId,
              ),
              Container(
                child: Center(
                  child: Feed()
                ),
              ),
              InstituteList(
                type: UniListItemType.campus,
                uniId: widget.uniId,
              ),
              InstituteList(
                type: UniListItemType.college,
              ),
              InstituteList(
                type: UniListItemType.department,
              ),
              InstituteList(
                type: UniListItemType.course,
              ),
              UniversityReview(),
            ],
          )),
    );
  }
}
