import 'package:Unihub/view/theme/colors.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:Unihub/view/theme/styles.dart';

class InstituteProfile {
  Widget getInstituteProfile(
      {String institute,
      String description,
      ImageProvider<Object> image,
      double rating,
      String about,
      int followers}) {
    return Column(
      children: [
        SizedBox(
          height: 15.0,
        ),
        Text(
          institute,
          style: titleTextStyle,
        ),
        Text(description ?? ''),
        SizedBox(
          height: 15.0,
        ),
        Visibility(
          visible: image != null,
          child: CircularProfileAvatar(
            'https://www.pavilionweb.com/wp-content/uploads/2017/03/man-300x300.png',
            child: image != null
                ? Image(
                    image: image,
                  )
                : null,
            radius: 70,
            backgroundColor: Colors.transparent,
            borderWidth: 0.0,
            borderColor: Colors.brown,
            elevation: 5.0,
            foregroundColor: Colors.brown.withOpacity(0.5),
            cacheImage: true,
            showInitialTextAbovePicture: true,
          ),
        ),
        SizedBox(
          height: 10.0,
        ),
        Visibility(
          visible: rating != null,
          child: RatingBar.builder(
            ignoreGestures: true,
            initialRating: rating,
            minRating: 1,
            direction: Axis.horizontal,
            allowHalfRating: true,
            itemCount: 5,
            itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
            itemBuilder: (context, _) => Icon(
              Icons.star,
              color: menuFontColor,
            ),
            onRatingUpdate: (double value) {},
          ),
        ),
        SizedBox(
          height: 8.0,
        ),
        Divider(color: Colors.black),
        SizedBox(
          height: 8.0,
        ),
        Text(
          about ?? '',
          textAlign: TextAlign.justify,
        ),
        SizedBox(
          height: 15.0,
        ),
        Visibility(
          visible: followers != null,
          child: Row(
            children: [
              Text(
                'followers'.tr,
                style: subtitleTextStyle,
              ),
              Spacer(),
              Text(
                followers.toString(),
                style: TextStyle(fontSize: 16.0),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 8.0,
        ),
        Divider(color: Colors.black),
        SizedBox(
          height: 8.0,
        ),
      ],
    );
  }
}
