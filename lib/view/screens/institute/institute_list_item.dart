import 'package:Unihub/util/constants.dart';
import 'package:Unihub/view/screens/campus_profile/campus_profile_tab.dart';
import 'package:Unihub/view/screens/college_profile/college_profile_tab.dart';
import 'package:Unihub/view/screens/course_profile/course_profile_tab.dart';
import 'package:Unihub/view/screens/course_profile/subject.dart';
import 'package:Unihub/view/screens/department_profile/department_profile_tab.dart';
import 'package:Unihub/view/screens/department_profile/lab.dart';
import 'package:Unihub/view/screens/university_profile/university_profile_tab.dart';
import 'package:Unihub/view/theme/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class InstituteListItem extends StatelessWidget {
  final String title, optionalTitle, optionalTitle2, description;
  final bool isIconVisible;
  final UniListItemType type;
  final int id;

  InstituteListItem(
      {this.title,
      this.optionalTitle,
      this.optionalTitle2,
      this.description,
      this.isIconVisible,
      this.type,
      this.id});

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2.0,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ListTile(
            trailing: Visibility(
              visible: isIconVisible ?? false,
              child: Icon(Icons.album),
            ),
            title: Text(title ?? ''),
            subtitle:
                Text((optionalTitle ?? '') + '\n' + (optionalTitle2 ?? '')),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15.0, right: 15.0),
            child: Text(
              description ?? '',
              textAlign: TextAlign.justify,
              maxLines: 3,
            ),
          ),
          SizedBox(width: 15.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              ElevatedButton(
                child: Text('message'.tr),
                onPressed: () {
                  /* ... */
                },
                style: ElevatedButton.styleFrom(
                  primary: primaryGreen,
                ),
              ),
              const SizedBox(width: 8),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: primaryGreen,
                ),
                child: Text('more_info'.tr),
                onPressed: () {
                  if (type == UniListItemType.campus) {
                    Get.to(CampusProfileTab(
                      campusId: id,
                    ));
                  } else if (type == UniListItemType.college) {
                    Get.to(CollegeProfileTab(
                      collegeId: id,
                    ));
                  } else if (type == UniListItemType.department) {
                    Get.to(DepartmentProfileTab(
                      depId: id,
                    ));
                  } else if (type == UniListItemType.course) {
                    Get.to(CourseProfileTab(
                      courseId: id,
                    ));
                  } else if (type == UniListItemType.lab) {
                    Get.to(Lab(
                      id: id,
                    ));
                  } else if (type == UniListItemType.subject) {
                    Get.to(Subject(
                      id: id,
                    ));
                  } else if (type == UniListItemType.university) {
                    Get.to(UniversityProfileTab(
                      uniId: id,
                    ));
                  } else {
                    print('Wrong type');
                  }
                },
              ),
              const SizedBox(width: 8),
            ],
          ),
        ],
      ),
    );
  }
}
