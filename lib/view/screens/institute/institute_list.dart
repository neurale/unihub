import 'package:Unihub/controller/campus_list_controller.dart';
import 'package:Unihub/controller/college_list_controller.dart';
import 'package:Unihub/controller/course_list_controller.dart';
import 'package:Unihub/controller/department_list_controller.dart';
import 'package:Unihub/controller/subject_list_controller.dart';
import 'package:Unihub/controller/university_list_controller.dart';
import 'package:Unihub/util/constants.dart';
import 'package:Unihub/view/widgets/custom/custom_widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:Unihub/view/screens/institute/institute_list_item.dart';
import 'package:get/get.dart';
import 'package:Unihub/controller/lab_list_controller.dart';
import 'package:get_storage/get_storage.dart';

class InstituteList extends StatefulWidget {
  final UniListItemType type;
  final int uniId;
  final int collegeId;
  final int labId;
  final int depId;

  InstituteList(
      {this.type, this.uniId, this.collegeId, this.labId, this.depId});

  @override
  _InstituteListState createState() => _InstituteListState();
}

class _InstituteListState extends State<InstituteList> {
  var _listController;
  String token;
  final _storage = GetStorage();

  @override
  void initState() {
    token = _storage.read(accessTokenKey);
    if (widget.type == UniListItemType.campus) {
      _listController = Get.put(CampusListController(token, widget.uniId.toString()));
    } else if (widget.type == UniListItemType.college) {
      _listController = Get.put(CollegeListController(token, widget.uniId.toString()));
    } else if (widget.type == UniListItemType.department) {
      _listController = Get.put(DepartmentListController(token, widget.uniId.toString()));
    } else if (widget.type == UniListItemType.course) {
      _listController = Get.put(CourseListController(token));
    } else if (widget.type == UniListItemType.lab) {
      _listController = Get.put(LabListController(token));
    } else if (widget.type == UniListItemType.subject) {
      _listController = Get.put(SubjectListController(token));
    } else if (widget.type == UniListItemType.university) {
      _listController = Get.put(UniversityListController(token));
    } else {
      print('Wrong type');
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding:
            EdgeInsets.only(left: 8.0, right: 8.0, bottom: 24.0, top: 24.0),
        child: Column(
          children: [
            Expanded(
              child: Obx(() {
                if (_listController.isLoading.value) {
                  return CustomWidget().getLoadingDualRing();
                } else {
                  if (widget.type == UniListItemType.campus) {
                    var filteredCampusList = _listController.campusList
                        .where((campus) =>
                            campus.universityId != null &&
                            campus.universityId == widget.uniId)
                        .toList();

                    return ListView.builder(
                        itemCount: filteredCampusList.length,
                        itemBuilder: (context, index) {
                          return InstituteListItem(
                            title: filteredCampusList[index].campusName,
                            description: filteredCampusList[index].summary,
                            isIconVisible: true,
                            optionalTitle2: filteredCampusList[index].tagline,
                            optionalTitle: filteredCampusList[index].city,
                            id: filteredCampusList[index].id,
                            type: widget.type,
                          );
                        });
                  } else if (widget.type == UniListItemType.college) {
                    return ListView.builder(
                        itemCount: _listController.collegeList.length,
                        itemBuilder: (context, index) {
                          return InstituteListItem(
                            title:
                                _listController.collegeList[index].collegeName,
                            description:
                                _listController.collegeList[index].summary,
                            isIconVisible: true,
                            optionalTitle2:
                                _listController.collegeList[index].tagline,
                            optionalTitle: _listController.collegeList[index].id
                                .toString(),
                            type: widget.type,
                            id: _listController.collegeList[index].id,
                          );
                        });
                  } else if (widget.type == UniListItemType.department) {
                    return ListView.builder(
                        itemCount: _listController.departmentList.length,
                        itemBuilder: (context, index) {
                          return InstituteListItem(
                            title: _listController
                                .departmentList[index].departmentName,
                            description:
                                _listController.departmentList[index].summary,
                            isIconVisible: true,
                            optionalTitle2:
                                _listController.departmentList[index].tagline,
                            optionalTitle: _listController
                                .departmentList[index].id
                                .toString(),
                            type: widget.type,
                            id: _listController.departmentList[index].id,
                          );
                        });
                  } else if (widget.type == UniListItemType.course) {
                    return ListView.builder(
                        itemCount: _listController.courseList.length,
                        itemBuilder: (context, index) {
                          return InstituteListItem(
                            title: _listController.courseList[index].courseName,
                            description:
                                _listController.courseList[index].summary,
                            isIconVisible: true,
                            optionalTitle2:
                                _listController.courseList[index].tagline,
                            optionalTitle: _listController
                                .courseList[index].courseFee
                                .toString(),
                            type: widget.type,
                            id: _listController.courseList[index].id,
                          );
                        });
                  } else if (widget.type == UniListItemType.subject) {
                    return ListView.builder(
                        itemCount: _listController.subjectList.length,
                        itemBuilder: (context, index) {
                          return InstituteListItem(
                            title:
                                _listController.subjectList[index].subjectName,
                            description:
                                _listController.subjectList[index].summary,
                            isIconVisible: true,
                            optionalTitle2:
                                _listController.subjectList[index].tagline,
                            optionalTitle:
                                _listController.subjectList[index].syllabus,
                            type: widget.type,
                            id: _listController.subjectList[index].id,
                          );
                        });
                  } else if (widget.type == UniListItemType.university) {
                    return ListView.builder(
                        itemCount: _listController.universityList.length,
                        itemBuilder: (context, index) {
                          return InstituteListItem(
                            title: _listController
                                .universityList[index].universityName,
                            description:
                                _listController.universityList[index].summary,
                            isIconVisible: true,
                            optionalTitle2:
                                _listController.universityList[index].tagline,
                            optionalTitle:
                                _listController.universityList[index].website,
                            type: widget.type,
                            id: _listController.universityList[index].id,
                          );
                        });
                  } else if (widget.type == UniListItemType.lab) {
                    return ListView.builder(
                        itemCount: _listController.labList.length,
                        itemBuilder: (context, index) {
                          return InstituteListItem(
                            title:
                                _listController.labList[index].labName,
                            description: _listController.labList[index].summary,
                            isIconVisible: true,
                            optionalTitle2:
                                _listController.labList[index].tagline,
                            optionalTitle:
                                _listController.labList[index].status.toString(),
                            type: widget.type,
                            id: _listController.labList[index].id,
                          );
                        });
                  } else {
                    return Container();
                  }
                }
              }),
            )
          ],
        ),
      ),
    );
  }
}
