import 'package:Unihub/util/constants.dart';
import 'package:Unihub/view/screens/campus_profile/lifestyle.dart';
import 'package:Unihub/view/screens/institute/institute_list.dart';
import 'package:Unihub/view/screens/university_profile/university_profile_review.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:Unihub/view/screens/campus_profile/campus_profile_about.dart';

class CampusProfileTab extends StatefulWidget {
  final int campusId;

  const CampusProfileTab({Key key, this.campusId}) : super(key: key);

  @override
  _CampusProfileTabState createState() => _CampusProfileTabState();
}

class _CampusProfileTabState extends State<CampusProfileTab> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            bottom: PreferredSize(
                child: TabBar(
                    isScrollable: true,
                    unselectedLabelColor: Colors.white.withOpacity(0.3),
                    indicatorColor: Colors.white,
                    tabs: [
                      Tab(
                        child: Text('about'.tr),
                      ),
                      Tab(
                        child: Text('colleges'.tr),
                      ),
                      Tab(
                        child: Text('lifestyle'.tr),
                      ),
                      Tab(
                        child: Text('reviews'.tr),
                      ),
                    ]),
                preferredSize: Size.fromHeight(30.0)),
          ),
          body: TabBarView(
            children: <Widget>[
              CampusProfileAbout(
                campusId: widget.campusId,
              ),
              InstituteList(
                type: UniListItemType.college,
              ),
              Container(
                child: Center(
                  child: LifeStyle()
                ),
              ),
              UniversityReview(),
            ],
          )),
    );
  }
}
