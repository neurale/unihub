import 'dart:io';

import 'package:Unihub/view/screens/university_profile/image_gallery.dart';
import 'package:Unihub/view/screens/institute/institute_profile.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:Unihub/view/theme/styles.dart';
import 'package:Unihub/view/widgets/custom/custom_widgets.dart';
import 'package:Unihub/controller/campus_info_controller.dart';

class CampusProfileAbout extends StatefulWidget {
  final int campusId;

  const CampusProfileAbout({Key key, this.campusId}) : super(key: key);

  @override
  _CampusProfileAboutState createState() => _CampusProfileAboutState();
}

class _CampusProfileAboutState extends State<CampusProfileAbout> {
  final GlobalKey<FormState> _formKeyValue = GlobalKey<FormState>();
  final picker = ImagePicker();
  File _image;
  final _customWidget = CustomWidget();
  CampusInfoController _controller;

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  @override
  void initState() {
    _controller = Get.put(CampusInfoController(widget.campusId.toString()));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: _formKeyValue,
        child: Obx(() {
          if (_controller.isLoading.value) {
            return CustomWidget().getLoadingDualRing();
          } else {
            return ListView(
              padding: EdgeInsets.only(left: 24.0, right: 24.0, bottom: 24.0),
              shrinkWrap: true,
              children: [
                Column(
                  children: [
                    InstituteProfile().getInstituteProfile(
                        institute: _controller.campusInfo.campusName,
                        description: _controller.campusInfo.tagline,
                        about: _controller.campusInfo.summary,
                        rating: 3,),
                    Text(
                      'overview'.tr,
                      style: subtitleTextStyle,
                    ),
                    SizedBox(
                      height: 15.0,
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: 120.0,
                          child: Text(
                            'address'.tr,
                            style: boldFontStyle,
                          ),
                        ),
                        _customWidget.getMultiLineTextField(
                            _controller.campusInfo.address),
                      ],
                    ),
                    SizedBox(
                      height: 8.0,
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: 120.0,
                          child: Text(
                            'contact_info'.tr,
                            style: boldFontStyle,
                          ),
                        ),
                        _customWidget.getMultiLineTextField('Information'),
                      ],
                    ),
                    SizedBox(
                      height: 8.0,
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: 120.0,
                          child: Text(
                            'parent_uni'.tr,
                            style: boldFontStyle,
                          ),
                        ),
                        _customWidget.getMultiLineTextField(
                            _controller.campusInfo.university.universityName),
                      ],
                    ),
                    SizedBox(
                      height: 15.0,
                    ),
                    Text(
                      'additional_info'.tr,
                      style: subtitleTextStyle,
                    ),
                    SizedBox(
                      height: 15.0,
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: 120.0,
                          child: Text(
                            'dorm_availability'.tr,
                            style: boldFontStyle,
                          ),
                        ),
                        _customWidget.getMultiLineTextField(
                            _controller.campusInfo.dormAvailability.toString()),
                      ],
                    ),
                    SizedBox(
                      height: 8.0,
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: 120.0,
                          child: Text(
                            'pictures'.tr,
                            style: boldFontStyle,
                          ),
                        ),
                        Column(
                          children: [
                            Row(
                              children: [
                                CachedNetworkImage(
                                  fit: BoxFit.fill,
                                  height: 100.0,
                                  width: 100.0,
                                  imageUrl: "https://placeimg.com/500/500/any",
                                  placeholder: (context, url) =>
                                      CircularProgressIndicator(),
                                  errorWidget: (context, url, error) =>
                                      Icon(Icons.error),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 4.0, bottom: 4.0),
                                  child: CachedNetworkImage(
                                    height: 100.0,
                                    width: 100.0,
                                    imageUrl:
                                        "https://placeimg.com/500/500/any",
                                    placeholder: (context, url) =>
                                        CircularProgressIndicator(),
                                    errorWidget: (context, url, error) =>
                                        Icon(Icons.error),
                                  ),
                                ),
                              ],
                            ),
                            CachedNetworkImage(
                              fit: BoxFit.fill,
                              height: 100.0,
                              width: 204.0,
                              imageUrl: "https://placeimg.com/500/500/any",
                              placeholder: (context, url) =>
                                  CircularProgressIndicator(),
                              errorWidget: (context, url, error) =>
                                  Icon(Icons.error),
                            ),
                            _customWidget.getCustomOutlinedButton(
                                buttonText: "see_more".tr,
                                onPressed: () => Get.to(Gallery())),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            );
          }
        }),
      ),
    );
  }
}
