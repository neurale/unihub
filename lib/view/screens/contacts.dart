import 'package:Unihub/view/screens/chat.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Contacts extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final title = 'SimpleContactList';

    return ListView(
      children: <Widget>[
        ListTile(
            leading: CircleAvatar(
              backgroundColor: Colors.white,
              backgroundImage: NetworkImage(
                  'https://i.pinimg.com/originals/51/f6/fb/51f6fb256629fc755b8870c801092942.png'),
            ),
            title: Text(
              'John Judah',
            ),
            subtitle: Text('2348031980943'),
            trailing: Icon(Icons.keyboard_arrow_right),
            onTap: () {
              Get.to(Chat());
            }),
        new Divider(
          height: 1.0,
          indent: 1.0,
        ),
        ListTile(
          leading: CircleAvatar(
            backgroundColor: Colors.white,
            backgroundImage: NetworkImage(
                'https://i.pinimg.com/originals/51/f6/fb/51f6fb256629fc755b8870c801092942.png'),
          ),
          title: Text('Bisola Akanbi'),
          subtitle: Text('2348031980943'),
          trailing: Icon(Icons.keyboard_arrow_right),
          onTap: () {
            Get.to(Chat());
          },
          onLongPress: () {
            Text('Data');
          },
        ),
        new Divider(
          height: 1.0,
          indent: 1.0,
        ),
        ListTile(
          leading: CircleAvatar(
            backgroundColor: Colors.white,
            backgroundImage: NetworkImage(
                'https://i.pinimg.com/originals/51/f6/fb/51f6fb256629fc755b8870c801092942.png'),
          ),
          title: Text('Eghosa Iku'),
          subtitle: Text('2348031980943'),
          trailing: Icon(Icons.keyboard_arrow_right),
          onTap: () {
            Get.to(Chat());
          },
        ),
        new Divider(
          height: 1.0,
          indent: 1.0,
        ),
        ListTile(
          leading: CircleAvatar(
            backgroundColor: Colors.white,
            backgroundImage: NetworkImage(
                'https://i.pinimg.com/originals/51/f6/fb/51f6fb256629fc755b8870c801092942.png'),
          ),
          title: Text(
            'Andrew Ndebuisi',
          ),
          subtitle: Text('2348034280943'),
          trailing: Icon(Icons.keyboard_arrow_right),
          onTap: () {
            Get.to(Chat());
          },
        ),
        new Divider(
          height: 1.0,
          indent: 1.0,
        ),
        ListTile(
          leading: CircleAvatar(
            backgroundColor: Colors.white,
            backgroundImage: NetworkImage(
                'https://i.pinimg.com/originals/51/f6/fb/51f6fb256629fc755b8870c801092942.png'),
          ),
          title: Text('Arinze Dayo'),
          subtitle: Text('2348031980943'),
          trailing: Icon(Icons.keyboard_arrow_right),
          onTap: () {
            Get.to(Chat());
          },
        ),
        new Divider(
          height: 1.0,
          indent: 1.0,
        ),
        ListTile(
          leading: CircleAvatar(
            backgroundColor: Colors.white,
            backgroundImage: NetworkImage(
                'https://i.pinimg.com/originals/51/f6/fb/51f6fb256629fc755b8870c801092942.png'),
          ),
          title: Text('Wakara Zimbu'),
          subtitle: Text('2348031980943'),
          trailing: Icon(Icons.keyboard_arrow_right),
          onTap: () {
            Get.to(Chat());
          },
        ),
        new Divider(
          height: 1.0,
          indent: 1.0,
        ),
        ListTile(
          leading: CircleAvatar(
            backgroundColor: Colors.white,
            backgroundImage: NetworkImage(
                'https://i.pinimg.com/originals/51/f6/fb/51f6fb256629fc755b8870c801092942.png'),
          ),
          title: Text('Emaechi Chinedu'),
          subtitle: Text('2348031980943'),
          trailing: Icon(Icons.keyboard_arrow_right),
          onTap: () {
            Get.to(Chat());
          },
        ),
        new Divider(
          height: 1.0,
          indent: 10.0,
        ),
        ListTile(
          leading: CircleAvatar(
            backgroundColor: Colors.white,
            backgroundImage: NetworkImage(
                'https://i.pinimg.com/originals/51/f6/fb/51f6fb256629fc755b8870c801092942.png'),
          ),
          title: Text('Osaretin Igbinomwanhia'),
          subtitle: Text('2348031980943'),
          trailing: Icon(Icons.keyboard_arrow_right),
        ),
        new Divider(
          height: 1.0,
          indent: 10.0,
        ),
        ListTile(
          leading: CircleAvatar(
            backgroundColor: Colors.white,
            backgroundImage: NetworkImage(
                'https://i.pinimg.com/originals/51/f6/fb/51f6fb256629fc755b8870c801092942.png'),
          ),
          title: Text('Osagumwenro Nosa'),
          subtitle: Text('2348031980943'),
          trailing: Icon(Icons.keyboard_arrow_right),
          onTap: () {
            Get.to(Chat());
          },
        ),
      ],
    );
  }
}
