import 'package:Unihub/view/screens/student_signup/student_signup_02.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';
import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import 'package:get/get.dart';
import 'package:flutter/services.dart';

class StudentSignup01 extends StatefulWidget {
  static String tag = 'login-page';

  @override
  _StudentSignup01State createState() => new _StudentSignup01State();
}

class _StudentSignup01State extends State<StudentSignup01> {
  final GlobalKey<FormState> _formKeyValue = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final TextEditingController controller = TextEditingController();
  String initialCountry = 'NG';
  PhoneNumber number = PhoneNumber(isoCode: 'NG');

  File _image;
  final picker = ImagePicker();

  // String name;
  String email;
  String pass;
  bool _isLoading;
  String dob = "DOB";

  @override
  void initState() {
    dob = "DOB";
  }

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  void getPhoneNumber(String phoneNumber) async {
    PhoneNumber number =
        await PhoneNumber.getRegionInfoFromPhoneNumber(phoneNumber, 'US');

    setState(() {
      this.number = number;
    });
  }

  @override
  Widget build(BuildContext context) {
    DateTime date = DateTime(1900);

    final logo = Hero(
      tag: 'hero',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 48.0,
        child: Image.network(
            'https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__340.jpg'),
      ),
    );

    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        body: Form(
          key: _formKeyValue,
          child: Center(
            child: ListView(
              shrinkWrap: true,
              padding: EdgeInsets.only(left: 24.0, right: 24.0),
              children: <Widget>[
                Center(
                  child: CircularProfileAvatar(
                    'https://www.pavilionweb.com/wp-content/uploads/2017/03/man-300x300.png',
                    child: _image != null
                        ? Image(
                            image: FileImage(_image),
                          )
                        : null,
                    radius: 70,
                    backgroundColor: Colors.transparent,
                    borderWidth: 0.0,
                    // initialsText: Text(
                    //   "AD",
                    //   style: TextStyle(fontSize: 40, color: Colors.white),
                    // ),
                    borderColor: Colors.brown,
                    elevation: 5.0,
                    foregroundColor: Colors.brown.withOpacity(0.5),
                    cacheImage: true,
                    onTap: () {
                      getImage();
                    },
                    showInitialTextAbovePicture: true,
                  ),
                ),

                SizedBox(height: 48.0),
                TextFormField(
                  onSaved: (String value) {
                    setState(() {
                      email = value;
                    });
                  },
                  keyboardType: TextInputType.emailAddress,
                  autofocus: false,
                  // initialValue: 'aaaa',
                  decoration: InputDecoration(
                    hintText: 'First Name',
                    contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                    // border: OutlineInputBorder(
                    //     borderRadius: BorderRadius.circular(32.0)),
                  ),
                ),
                SizedBox(height: 8.0),
                TextFormField(
                  onSaved: (String value) {
                    setState(() {
                      pass = value;
                    });
                  },
                  autofocus: false,
                  // initialValue: 'abc',
                  obscureText: true,
                  decoration: InputDecoration(
                    hintText: 'Last Name',
                    contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                    // border: OutlineInputBorder(
                    //     borderRadius: BorderRadius.circular(32.0)),
                  ),
                ),
                SizedBox(height: 8.0),
                TextFormField(
                  onSaved: (String value) {
                    if (mounted) {
                      setState(() {
                        email = value;
                      });
                    }
                  },
                  keyboardType: TextInputType.name,
                  autofocus: false,
                  // initialValue: 'aaaa',
                  decoration: InputDecoration(
                    hintText: '@User Name',
                    contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                    // border: OutlineInputBorder(
                    //     borderRadius: BorderRadius.circular(32.0)),
                  ),
                ),
                SizedBox(height: 8.0),
                TextFormField(
                  // onSaved: (String value) {},
                  onTap: () async {
                    FocusScope.of(context).requestFocus(new FocusNode());

                    date = await showDatePicker(
                        context: context,
                        initialDate: DateTime.now(),
                        firstDate: DateTime(1900),
                        lastDate: DateTime(2100));
                    controller.text = date.toIso8601String();
                    setState(() {
                      dob = controller.text;
                    });
                    print(dob);
                  },
                  keyboardType: TextInputType.name,
                  autofocus: false,
                  // initialValue: 'aaaa',
                  decoration: InputDecoration(
                    hintText: dob,
                    labelText: dob,
                    contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                    // border: OutlineInputBorder(
                    //     borderRadius: BorderRadius.circular(32.0)),
                  ),
                ),
                SizedBox(height: 8.0),

                // Divider(color: Colors.black),
                SizedBox(height: 8.0),
                TextFormField(
                  onSaved: (String value) {
                    setState(() {
                      pass = value;
                    });
                  },
                  autofocus: false,
                  // initialValue: 'abc',
                  obscureText: true,
                  decoration: InputDecoration(
                    hintText: 'Adddress',
                    contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                    // border: OutlineInputBorder(
                    //     borderRadius: BorderRadius.circular(32.0)),
                  ),
                ),
                SizedBox(height: 8.0),

                InternationalPhoneNumberInput(
                  onInputChanged: (PhoneNumber number) {
                    print(number.phoneNumber);
                  },
                  onInputValidated: (bool value) {
                    print(value);
                  },
                  selectorConfig: SelectorConfig(
                    selectorType: PhoneInputSelectorType.BOTTOM_SHEET,
                  ),
                  ignoreBlank: false,
                  autoValidateMode: AutovalidateMode.disabled,
                  selectorTextStyle: TextStyle(color: Colors.black),
                  initialValue: number,
                  textFieldController: controller,
                  formatInput: false,
                  keyboardType: TextInputType.numberWithOptions(
                      signed: true, decimal: true),
                  // inputBorder: OutlineInputBorder(),
                  onSaved: (PhoneNumber number) {
                    print('On Saved: $number');
                  },
                ),
                SizedBox(height: 8.0),
                TextFormField(
                  onSaved: (String value) {
                    if (mounted) {
                      setState(() {
                        email = value;
                      });
                    }
                  },
                  keyboardType: TextInputType.name,
                  autofocus: false,
                  // initialValue: 'aaaa',
                  decoration: InputDecoration(
                    hintText: 'City',
                    contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                    // border: OutlineInputBorder(
                    //     borderRadius: BorderRadius.circular(32.0)),
                  ),
                ),
                SizedBox(height: 8.0),
                TextFormField(
                  onSaved: (String value) {
                    if (mounted) {
                      setState(() {
                        email = value;
                      });
                    }
                  },
                  keyboardType: TextInputType.name,
                  autofocus: false,
                  // initialValue: 'aaaa',
                  decoration: InputDecoration(
                    hintText: 'State',
                    contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                    // border: OutlineInputBorder(
                    //     borderRadius: BorderRadius.circular(32.0)),
                  ),
                ),

                SizedBox(height: 24.0),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 16.0),
                  child: ElevatedButton(
                    // shape: RoundedRectangleBorder(
                    //   borderRadius: BorderRadius.circular(24),
                    // ),
                    onPressed: () {
                      // login();
                      // getImage();
                      Get.to(StudentSignup02());
                      print("hello");
                    },
                    // padding: EdgeInsets.all(12),
                    // color: Colors.lightBlueAccent,
                    child: Text('Next', style: TextStyle(color: Colors.white)),
                  ),
                ),

                // forgotLabel
              ],
            ),
          ),
        ));
  }

  @override
  void dispose() {
    // TODO: implement dispose
    controller?.dispose();
    super.dispose();
  }
}
