import 'package:Unihub/view/theme/colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:search_choices/search_choices.dart';
import 'package:Unihub/view/screens/search/search_result.dart';

class SearchPage extends StatefulWidget {
  static final navKey = new GlobalKey<NavigatorState>();

  const SearchPage({Key navKey}) : super(key: navKey);

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  String studyLevel;
  String subjectOfInterest;
  String studyDestination;
  List<DropdownMenuItem> items = [];
  String inputString = "";
  TextFormField input;
  final searchTextController = TextEditingController();
  final String loremIpsum =
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

  @override
  void initState() {
    String wordPair = "";
    loremIpsum
        .toLowerCase()
        .replaceAll(",", "")
        .replaceAll(".", "")
        .split(" ")
        .forEach((word) {
      if (wordPair.isEmpty) {
        wordPair = word + " ";
      } else {
        wordPair += word;
        if (items.indexWhere((item) {
              return (item.value == wordPair);
            }) ==
            -1) {
          items.add(DropdownMenuItem(
            child: Text(wordPair),
            value: wordPair,
          ));
        }
        wordPair = "";
      }
    });
    input = TextFormField(
      validator: (value) {
        return ((value?.length ?? 0) < 6
            ? "must be at least 6 characters long"
            : null);
      },
      initialValue: inputString,
      onChanged: (value) {
        inputString = value;
      },
      autofocus: true,
    );

    searchTextController.addListener(() {
      setState(() {});
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(children: [
          Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
              side: BorderSide(
                color: Colors.grey,
                width: 1.0,
              ),
            ),
            margin: EdgeInsets.only(top: 15.0, left: 15.0, right: 15.0),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: <Widget>[
                  SearchChoices.single(
                    items: items,
                    value: studyLevel,
                    hint: 'study_level'.tr,
                    onChanged: (value) {
                      setState(() {
                        studyLevel = value;
                      });
                    },
                    isExpanded: true,
                  ),
                ],
              ),
            ),
          ),
          Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
              side: BorderSide(
                color: Colors.grey,
                width: 1.0,
              ),
            ),
            margin: EdgeInsets.only(top: 15.0, left: 15.0, right: 15.0),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: <Widget>[
                  SearchChoices.single(
                    items: items,
                    value: subjectOfInterest,
                    hint: 'subject_of_interest'.tr,
                    onChanged: (value) {
                      setState(() {
                        subjectOfInterest = value;
                      });
                    },
                    isExpanded: true,
                  ),
                ],
              ),
            ),
          ),
          Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
              side: BorderSide(
                color: Colors.grey,
                width: 1.0,
              ),
            ),
            margin: EdgeInsets.only(top: 15.0, left: 15.0, right: 15.0),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: <Widget>[
                  SearchChoices.single(
                    items: items,
                    value: studyDestination,
                    hint: 'study_destination'.tr,
                    onChanged: (value) {
                      setState(() {
                        studyDestination = value;
                      });
                    },
                    isExpanded: true,
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
            child: ElevatedButton.icon(
              style: ElevatedButton.styleFrom(
                primary: primaryGreen,
              ),
              onPressed: () {
                Get.to(SearchResult());
              },
              icon: Icon(Icons.search),
              label: Text('search'.tr),
            ),
          ),
          Row(children: <Widget>[
            Expanded(
              child: new Container(
                  margin: const EdgeInsets.only(left: 10.0, right: 20.0),
                  child: Divider(
                    color: Colors.black,
                    height: 36,
                  )),
            ),
            Text('or'.tr),
            Expanded(
              child: new Container(
                  margin: const EdgeInsets.only(left: 20.0, right: 10.0),
                  child: Divider(
                    color: Colors.black,
                    height: 36,
                  )),
            ),
          ]),
          Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
              side: BorderSide(
                color: Colors.grey,
                width: 1.0,
              ),
            ),
            margin: EdgeInsets.only(top: 15.0, left: 15.0, right: 15.0),
            child: Padding(
              padding: const EdgeInsets.only(
                  top: 8.0, bottom: 8.0, left: 15.0, right: 15.0),
              child: TextField(
                controller: searchTextController,
                decoration: InputDecoration(
                  focusedBorder: InputBorder.none,
                  border: InputBorder.none,
                  suffixIcon: IconButton(
                    icon: Icon(Icons.search),
                    onPressed: () {
                      if (searchTextController.text.length > 0) {
                        Get.to(SearchResult());
                      }
                    },
                  ),
                  prefixIcon: searchTextController.text.length > 0
                      ? IconButton(
                          icon: Icon(Icons.close),
                          onPressed: () {
                            setState(() {
                              searchTextController.clear();
                            });
                          },
                        )
                      : null,
                  hintText: 'site_search'.tr,
                ),
              ),
            ),
          ),
        ]),
      ),
    );
  }
}
