import 'package:Unihub/view/screens/institute/institute_list_item.dart';
import 'package:Unihub/view/theme/colors.dart';
import 'package:Unihub/view/theme/styles.dart';
import 'package:Unihub/view/widgets/custom/custom_widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:chips_choice/chips_choice.dart';
import 'package:get/get.dart';

class SearchResult extends StatefulWidget {
  @override
  _SearchResultState createState() => _SearchResultState();
}

class _SearchResultState extends State<SearchResult> {
  final List<InstituteListItem> campusList = [
    InstituteListItem(),
    InstituteListItem(),
    InstituteListItem(),
    InstituteListItem(),
    InstituteListItem(),
    InstituteListItem(),
  ];

  int choice = 0;

  List<String> options = [
    'all_search_results'.tr,
    'universities'.tr,
    'colleges'.tr,
    'departments'.tr,
    'courses'.tr,
  ];

  filterResult(int selectedIndex) {
    switch (selectedIndex) {
      case 0:
        {
          print('0');
        }
        break;
      case 1:
        {
          print('1');
        }
        break;
      case 2:
        {
          print('2');
        }
        break;
      case 3:
        {
          print('3');
        }
        break;
      case 4:
        {
          print('4');
        }
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Column(
        children: [
          Card(
            elevation: 5.0,
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.only(top: 15.0, bottom: 8.0),
                  child: Text(
                    'Search Filters',
                    style: titleTextStyle,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    left: 15.0,
                    bottom: 5.0,
                  ),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      'Price Range',
                      style: subtitleTextStyle,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 15.0, right: 15.0, bottom: 10.0),
                  child: Row(
                    children: [
                      Expanded(
                        child: Container(
                          height: 30.0,
                          padding: EdgeInsets.only(right: 4.0),
                          child: TextField(
                            textAlign: TextAlign.center,
                            decoration: InputDecoration(
                              contentPadding:
                                  EdgeInsets.symmetric(vertical: 0.0),
                              focusedBorder: OutlineInputBorder(
                                borderSide: const BorderSide(
                                    color: primaryGreen, width: 1.0),
                                borderRadius: BorderRadius.circular(8.0),
                              ),
                              hintText: 'min'.tr,
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8.0)),
                            ),
                            keyboardType: TextInputType.number,
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          height: 30.0,
                          padding: EdgeInsets.only(left: 4.0),
                          child: TextField(
                            textAlign: TextAlign.center,
                            decoration: InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                borderSide: const BorderSide(
                                    color: primaryGreen, width: 1.0),
                                borderRadius: BorderRadius.circular(8.0),
                              ),
                              hintText: 'max'.tr,
                              contentPadding:
                                  EdgeInsets.symmetric(vertical: 0.0),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8.0)),
                            ),
                            keyboardType: TextInputType.number,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                ChipsChoice<int>.single(
                  value: choice,
                  onChanged: (val) {
                    setState(() {
                      choice = val;
                      filterResult(val);
                    });
                  },
                  choiceItems: C2Choice.listFrom<int, String>(
                    source: options,
                    value: (i, v) => i,
                    label: (i, v) => v,
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: campusList == null
                ? CustomWidget().getLoadingDualRing()
                : ListView.builder(
                    itemCount: campusList.length,
                    itemBuilder: (context, index) {
                      return InstituteListItem(
                        title: 'Title',
                        description:
                            'Lorem ipsum dolor sit amet, per quod audire an. Nulla epicurei mel ut, ea qui summo accumsan torquatos, vim ad vidisse saperet atomorum. Pro postea pericula et, te duo nulla mollis adversarium, te noluisse convenire sea. Meis iriure ut usu, tollit latine adversarium eu cum. Graeci efficiantur ea eam.',
                        isIconVisible: true,
                        optionalTitle2: 'Title2',
                        optionalTitle: 'Title',
                      );
                    }),
          )
        ],
      ),
    );
  }
}
