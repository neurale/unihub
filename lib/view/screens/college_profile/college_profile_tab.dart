import 'package:Unihub/util/constants.dart';
import 'package:Unihub/view/screens/institute/institute_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:Unihub/view/screens/college_profile/college_profile_about.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CollegeProfileTab extends StatefulWidget {
  final int collegeId;

  const CollegeProfileTab({Key key, this.collegeId}) : super(key: key);

  @override
  _CollegeProfileTabState createState() => _CollegeProfileTabState();
}

class _CollegeProfileTabState extends State<CollegeProfileTab> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            bottom: PreferredSize(
                child: TabBar(
                    isScrollable: true,
                    unselectedLabelColor: Colors.white.withOpacity(0.3),
                    indicatorColor: Colors.white,
                    tabs: [
                      Tab(
                        child: Text('about'.tr),
                      ),
                      Tab(
                        child: Text('departments'.tr),
                      ),
                    ]),
                preferredSize: Size.fromHeight(30.0)),
          ),
          body: TabBarView(
            children: <Widget>[
              CollegeProfileAbout(
                collegeId: widget.collegeId,
              ),
              InstituteList(
                type: UniListItemType.department,
              ),
            ],
          )),
    );
  }
}
