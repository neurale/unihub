import 'package:Unihub/view/screens/single_feed.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Feed extends StatefulWidget {
  static String tag = 'login-page';

  @override
  _FeedState createState() => new _FeedState();
}

class _FeedState extends State<Feed> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: new Container(
      color: Colors.white,
      child: new ListView(
        children: <Widget>[
          _feedCard(context),
          _feedCard(context),
          _feedCard(context)
        ],
      ),
    ));
  }
}

Widget _feedCard(BuildContext context) {
  return Padding(
    padding: const EdgeInsets.only(top: 3),
    child: Card(
      clipBehavior: Clip.antiAlias,
      elevation: 2.0,
      child: Column(
        children: [
          ListTile(
            leading: CircleAvatar(
              backgroundImage: NetworkImage(
                  'https://library.kissclipart.com/20180918/hyw/kissclipart-universities-icons-clipart-university-clip-art-f9ea22390606b78a.jpg'),
            ),
            title: const Text('UCSC'),
            subtitle: Text(
              'The best',
              style: TextStyle(color: Colors.black.withOpacity(0.6)),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(
              'Greyhound divisively hello coldly wonderfully marginally far upon excluding.',
              style: TextStyle(color: Colors.black.withOpacity(0.6)),
            ),
          ),
          Image.network(
              'https://images.pexels.com/photos/1563356/pexels-photo-1563356.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500'),
          ButtonBar(
            alignment: MainAxisAlignment.start,
            children: [
              FlatButton(
                onPressed: () {
                  // Perform some action
                },
                child: const Text('Like'),
              ),
              FlatButton(
                onPressed: () {
                  Get.to(SingleFeed());
                },
                child: const Text('Comment'),
              ),
              FlatButton(
                onPressed: () {
                  // Perform some action
                },
                child: const Text('Share'),
              ),
            ],
          ),
          InkWell(
            onTap: () {
              Get.to(SingleFeed());
            },
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                children: [
                  Row(
                    children: [
                      CircleAvatar(
                        backgroundImage: NetworkImage(
                            'https://cdn4.iconfinder.com/data/icons/avatars-xmas-giveaway/128/girl_female_woman_avatar-512.png'),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text('Nimali Wasana'),
                      ),
                      Text('2 m ago')
                    ],
                  ),
                  Text(
                    'Greyhound divisively hello coldly wonderfully marginally far upon excluding.Greyhound divisively hello coldly wonderfully marginally far upon excluding.',
                    style: TextStyle(color: Colors.black.withOpacity(0.6)),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    ),
  );
}
