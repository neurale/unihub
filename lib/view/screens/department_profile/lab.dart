import 'package:Unihub/view/theme/styles.dart';
import 'package:Unihub/view/widgets/custom/custom_widgets.dart';
import 'package:flutter/cupertino.dart';
import 'dart:io';
import 'package:Unihub/view/screens/institute/institute_profile.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class Lab extends StatefulWidget {
  final int id;

  const Lab({Key key, this.id}) : super(key: key);

  @override
  _LabState createState() => _LabState();
}

class _LabState extends State<Lab> {
  final GlobalKey<FormState> _formKeyValue = GlobalKey<FormState>();
  final picker = ImagePicker();
  File _image;
  final _customWidget = CustomWidget();

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Form(
          key: _formKeyValue,
          child: ListView(
            padding: EdgeInsets.only(left: 24.0, right: 24.0, bottom: 24.0),
            shrinkWrap: true,
            children: [
              Column(
                children: [
                  InstituteProfile().getInstituteProfile(
                      institute: 'Lab Name',
                      description: 'Description',),
                  Text(
                    'overview'.tr,
                    style: subtitleTextStyle,
                  ),
                  SizedBox(
                    height: 15.0,
                  ),
                  Row(
                    children: [
                      SizedBox(
                        width: 120.0,
                        child: Text(
                          'parent_college'.tr,
                          style: boldFontStyle,
                        ),
                      ),
                      _customWidget.getMultiLineTextField('Information'),
                    ],
                  ),
                  SizedBox(
                    height: 8.0,
                  ),
                  Row(
                    children: [
                      SizedBox(
                        width: 120.0,
                        child: Text(
                          'parent_deps'.tr,
                          style: boldFontStyle,
                        ),
                      ),
                      _customWidget.getMultiLineTextField('Information'),
                    ],
                  ),
                  SizedBox(
                    height: 8.0,
                  ),
                  Row(
                    children: [
                      SizedBox(
                        width: 120.0,
                        child: Text(
                          'available_campuses'.tr,
                          style: boldFontStyle,
                        ),
                      ),
                      _customWidget.getMultiLineTextField('Information'),
                    ],
                  ),
                ],
              ),
            ],
          )),
    );
  }
}
