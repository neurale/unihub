import 'package:Unihub/util/constants.dart';
import 'package:Unihub/view/screens/institute/institute_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:Unihub/view/screens/department_profile/department_profile_about.dart';

class DepartmentProfileTab extends StatefulWidget {
  final int depId;

  const DepartmentProfileTab({Key key, this.depId}) : super(key: key);

  @override
  _DepartmentProfileTabState createState() => _DepartmentProfileTabState();
}

class _DepartmentProfileTabState extends State<DepartmentProfileTab> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            bottom: PreferredSize(
                child: TabBar(
                    isScrollable: true,
                    unselectedLabelColor: Colors.white.withOpacity(0.3),
                    indicatorColor: Colors.white,
                    tabs: [
                      Tab(
                        child: Text('about'.tr),
                      ),
                      Tab(
                        child: Text('courses'.tr),
                      ),
                      Tab(
                        child: Text('research_labs'.tr),
                      ),
                    ]),
                preferredSize: Size.fromHeight(30.0)),
          ),
          body: TabBarView(
            children: <Widget>[
              DepartmentProfileAbout(
                depId: widget.depId,
              ),
              InstituteList(
                type: UniListItemType.course,
              ),
              InstituteList(
                type: UniListItemType.lab,
                depId: widget.depId,
              ),
            ],
          )),
    );
  }
}
