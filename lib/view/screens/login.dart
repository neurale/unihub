import 'package:Unihub/services/login_service.dart';
import 'package:Unihub/util/constants.dart';
import 'package:Unihub/view/screens/home.dart';
import 'package:flutter/material.dart';
import 'package:flutter_login/flutter_login.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class LoginScreen extends StatelessWidget {
  final _storage = GetStorage();

  Duration get loginTime => Duration(milliseconds: 2250);

  Future<String> _authUser(LoginData data) async {
    LoginService con = Get.put(LoginService());
    var info = await con.fetchLoginInfo(data.name, data.password);
    var token = info.body['access_token'];
    if (info.status.isOk && token != null) {
      _storage.write(accessTokenKey, token);
      return null;
    } else {
      return info.statusText;
    }
  }

  Future<String> _recoverPassword(String name) {
    print('Name: $name');
    return Future.delayed(loginTime).then((_) {
      return null;
    });
  }

  @override
  Widget build(BuildContext context) {
    return FlutterLogin(
      title: 'app_title'.tr,
      logo: null,
      onLogin: _authUser,
      onSignup: (_){},
      onSubmitAnimationCompleted: () => Get.off(Home()),
      onRecoverPassword: _recoverPassword,
    );
  }
}
