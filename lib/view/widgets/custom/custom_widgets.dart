import 'package:Unihub/view/theme/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';

class CustomWidget {
  Widget getEditPencil() {
    return new CircleAvatar(
      backgroundColor: primaryGreen,
      radius: 14.0,
      child: new Icon(
        Icons.edit,
        color: Colors.white,
        size: 16.0,
      ),
    );
  }

  Widget getLoadingDualRing() {
    return Container(
      color: Color(0x00FFFFFF),
      child: Center(
        child: SpinKitDualRing(
          color: primaryGreen,
          size: 50.0,
        ),
      ),
    );
  }

  Widget getMultiLineTextField(String text, {List<String> list}) {
    if (list != null && list.length > 0) {
      var output = StringBuffer();

      list.forEach((item) {
        output.write(item + '\n');
      });
      return Container(
        width: 220.0,
        child: Text(output.toString()),
      );
    }
    return Container(
      width: 220.0,
      child: Text(text ?? ''),
    );
  }

  Widget getCustomOutlinedButton({String buttonText, VoidCallback onPressed}) {
    return OutlinedButton(
      style: OutlinedButton.styleFrom(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(30))),
        side: BorderSide(color: primaryGreen, width: 1),
      ),
      onPressed: onPressed,
      child: Text(
        buttonText,
        style: TextStyle(color: primaryGreen),
      ),
    );
  }
}
