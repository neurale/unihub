import 'package:Unihub/view/theme/colors.dart';
import 'package:flutter/material.dart';

const titleTextStyle = TextStyle(
  fontSize: 18.0,
  fontWeight: FontWeight.bold,
  color: primaryGreen
);

const subtitleTextStyle = TextStyle(
  fontSize: 16.0,
  fontWeight: FontWeight.bold,
);

const boldFontStyle = TextStyle(
  fontWeight: FontWeight.bold,
);
