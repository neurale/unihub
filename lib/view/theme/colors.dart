import 'package:flutter/material.dart';

const primaryBlack = Color(0xFF444444);
const primaryGreen = Color(0xFF1e907e);
const fontColor = Color(0xFFffffff);
const menuFontColor = Color(0xFFffd923);
