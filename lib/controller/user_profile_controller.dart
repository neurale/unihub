import 'package:Unihub/model/user_profile.dart';
import 'package:Unihub/services/user_profile_service.dart';
import 'package:get/get.dart';

class UserProfileController extends GetxController {
  final String token;
  var isLoading = true.obs;
  UserProfileService _profileService;
  var profileData = UserProfile();

  UserProfileController(this.token);

  @override
  void onInit() {
    getUser(token);
    super.onInit();
  }

  void getUser(String token) async {
    try {
      isLoading(true);
      _profileService = Get.put(UserProfileService());
      profileData = await _profileService.getUser(token);
    } finally {
      isLoading(false);
    }
  }
}
