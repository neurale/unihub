import 'package:get/get.dart';
import 'package:Unihub/model/college.dart';
import 'package:Unihub/services/college_list_service.dart';

class CollegeListController extends GetxController {
  final String token;
  final String uniId;
  var isLoading = true.obs;

  List collegeList = <Datum>[].obs;

  CollegeListController(this.token, this.uniId);

  @override
  void onInit() {
    fetchCollegeList();
    super.onInit();
  }

  void fetchCollegeList() async {
    try {
      isLoading(true);
      var summary = await CollegeListService.fetchCampusList(token, uniId);
      collegeList = summary.data;
    } finally {
      isLoading(false);
    }
  }
}