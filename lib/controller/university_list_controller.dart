import 'package:get/get.dart';
import 'package:Unihub/services/university_list_service.dart';
import 'package:Unihub/model/university.dart';

class UniversityListController extends GetxController {
  final String token;
  var isLoading = true.obs;

  List universityList = <Datum>[].obs;

  UniversityListController(this.token);

  @override
  void onInit() {
    fetchUniversityList();
    super.onInit();
  }

  void fetchUniversityList() async {
    try {
      isLoading(true);
      var summary = await UniversityListService.fetchUniversityList(token);
      universityList = summary.data;
    } finally {
      isLoading(false);
    }
  }
}
