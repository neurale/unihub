import 'package:get/get.dart';
import 'package:Unihub/model/campus_info.dart';
import 'package:Unihub/services/campus_info_service.dart';

class CampusInfoController extends GetxController {
  final String id;
  var isLoading = true.obs;
  var campusInfo = Data();

  CampusInfoController(this.id);

  @override
  void onInit() {
    fetchUniversityInfo(id);
    super.onInit();
  }

  void fetchUniversityInfo(String id) async {
    try {
      isLoading(true);
      var response = await CampusInfoService.fetchCampusInfo(id);
      campusInfo = response.data;
    } on Exception catch (exception) {
      print(exception);
    } catch (error) {
      print(error);
    } finally {
      isLoading(false);
    }
  }
}
