import 'package:Unihub/model/student.dart';
import 'package:get/get.dart';
import 'package:Unihub/services/student_signup_service.dart';
import 'package:Unihub/model/signup_info.dart';

class StudentSignupController extends GetxController {
  final Student student;
  var isLoading = true.obs;
  SignupInfo signupInfo;

  StudentSignupController(this.student);

  @override
  void onInit() {
    fetchSignupInfo(student);
    super.onInit();
  }

  void fetchSignupInfo(Student student) async {
    try {
      isLoading(true);
      signupInfo = await StudentSignupService.fetchSignupInfo(student);
      print(signupInfo);
    } on Exception catch (exception) {
      print(exception);
    } catch (error) {
      print(error);
    } finally {
      isLoading(false);
    }
  }
}
