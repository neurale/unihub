import 'package:get/get.dart';
import 'package:Unihub/model/course_info.dart';
import 'package:Unihub/services/course_info_service.dart';

class CourseInfoController extends GetxController {
  final String id;
  var isLoading = true.obs;
  var courseInfo = Data();

  CourseInfoController(this.id);

  @override
  void onInit() {
    fetchCourseInfo(id);
    super.onInit();
  }

  void fetchCourseInfo(String id) async {
    try {
      isLoading(true);
      var response = await CourseInfoService.fetchCourseInfo(id);
      courseInfo = response.data;
    } on Exception catch (exception) {
      print(exception);
    } catch (error) {
      print(error);
    } finally {
      isLoading(false);
    }
  }
}
