import 'package:get/get.dart';
import 'package:Unihub/services/lab_list_service.dart';
import 'package:Unihub/model/lab.dart';

class LabListController extends GetxController {
  final String token;
  var isLoading = true.obs;

  List labList = <Datum>[].obs;

  LabListController(this.token);

  @override
  void onInit() {
    fetchLabList();
    super.onInit();
  }

  void fetchLabList() async {
    try {
      isLoading(true);
      var summary = await LabListService.fetchLabList(token);
      labList = summary.data;
    } finally {
      isLoading(false);
    }
  }
}
