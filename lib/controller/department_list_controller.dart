import 'package:get/get.dart';
import 'package:Unihub/model/department.dart';
import 'package:Unihub/services/department_list_service.dart';

class DepartmentListController extends GetxController {
  final String token;
  final String uniId;
  var isLoading = true.obs;

  List departmentList = <Datum>[].obs;

  DepartmentListController(this.token, this.uniId);

  @override
  void onInit() {
    fetchDpartmentList();
    super.onInit();
  }

  void fetchDpartmentList() async {
    try {
      isLoading(true);
      var summary = await DepartmentListService.fetchDepartmentList(token, uniId);
      departmentList = summary.data;
    } finally {
      isLoading(false);
    }
  }
}
