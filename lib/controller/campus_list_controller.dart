import 'package:get/get.dart';
import 'package:Unihub/services/campus_list_service.dart';
import 'package:Unihub/model/campus.dart';

class CampusListController extends GetxController {
  final String token;
  final String uniId;
  var isLoading = true.obs;

  List campusList = <Datum>[].obs;

  CampusListController(this.token, this.uniId);

  @override
  void onInit() {
    fetchCampusList();
    super.onInit();
  }

  void fetchCampusList() async {
    try {
      isLoading(true);
      var summary = await CampusListService.fetchCampusList(token, uniId);
      campusList = summary.data;
    } finally {
      isLoading(false);
    }
  }
}
