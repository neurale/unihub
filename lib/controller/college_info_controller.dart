import 'package:get/get.dart';
import 'package:Unihub/services/college_info_service.dart';
import 'package:Unihub/model/college_info.dart';

class CollegeInfoController extends GetxController {
  final String id;
  var isLoading = true.obs;
  var collegeInfo = Data();

  CollegeInfoController(this.id);

  @override
  void onInit() {
    fetchCollegeInfo(id);
    super.onInit();
  }

  void fetchCollegeInfo(String id) async {
    try {
      isLoading(true);
      var response = await CollegeInfoService.fetchCollegeInfo(id);
      collegeInfo = response.data;
    } on Exception catch (exception) {
      print(exception);
    } catch (error) {
      print(error);
    } finally {
      isLoading(false);
    }
  }
}
