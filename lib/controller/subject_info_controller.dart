import 'package:Unihub/model/subject_info.dart';
import 'package:Unihub/services/subject_info_service.dart';
import 'package:get/get.dart';

class SubjectInfoController extends GetxController {
  final String id;
  var isLoading = true.obs;
  var subjectInfo = Data();

  SubjectInfoController(this.id);

  @override
  void onInit() {
    fetchSubjectInfo(id);
    super.onInit();
  }

  void fetchSubjectInfo(String id) async {
    try {
      isLoading(true);
      var response = await SubjectInfoService.fetchSubjectInfo(id);
      subjectInfo = response.data;
    } on Exception catch (exception) {
      print(exception);
    } catch (error) {
      print(error);
    } finally {
      isLoading(false);
    }
  }
}
