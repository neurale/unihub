import 'package:Unihub/model/department_info.dart';
import 'package:Unihub/services/department_info_service.dart';
import 'package:get/get.dart';

class DepartmentInfoController extends GetxController {
  final String id;
  var isLoading = true.obs;
  var departmentInfo = Data();

  DepartmentInfoController(this.id);

  @override
  void onInit() {
    fetchDepartmentInfo(id);
    super.onInit();
  }

  void fetchDepartmentInfo(String id) async {
    try {
      isLoading(true);
      var response = await DepartmentInfoService.fetchDepartmentInfo(id);
      departmentInfo = response.data;
    } on Exception catch (exception) {
      print(exception);
    } catch (error) {
      print(error);
    } finally {
      isLoading(false);
    }
  }
}
