import 'package:Unihub/model/course.dart';
import 'package:Unihub/services/course_list_service.dart';
import 'package:get/get.dart';

class CourseListController extends GetxController {
  final String token;
  var isLoading = true.obs;

  List courseList = <Datum>[].obs;

  CourseListController(this.token);

  @override
  void onInit() {
    fetchCourseList();
    super.onInit();
  }

  void fetchCourseList() async {
    try {
      isLoading(true);
      var summary = await CourseListService.fetchCourseList(token);
      courseList = summary.data;
    } finally {
      isLoading(false);
    }
  }
}
