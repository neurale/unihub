import 'package:get/get.dart';
import 'package:Unihub/model/university_info.dart';
import 'package:Unihub/services/university_info_service.dart';

class UniversityInfoController extends GetxController {
  final String id;
  var isLoading = true.obs;
  var universityInfo = Data();

  UniversityInfoController(this.id);

  @override
  void onInit() {
    fetchUniversityInfo(id);
    super.onInit();
  }

  void fetchUniversityInfo(String id) async {
    try {
      isLoading(true);
      var response = await UniversityInfoService.fetchUniversityInfo(id);
      universityInfo = response.data;
    } on Exception catch (exception) {
      print(exception);
    } catch (error) {
      print(error);
    } finally {
      isLoading(false);
    }
  }
}
