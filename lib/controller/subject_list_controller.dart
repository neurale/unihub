import 'package:Unihub/model/subject.dart';
import 'package:Unihub/services/subject_list_service.dart';
import 'package:get/get.dart';

class SubjectListController extends GetxController {
  final String token;
  var isLoading = true.obs;

  List subjectList = <Datum>[].obs;

  SubjectListController(this.token);

  @override
  void onInit() {
    fetchSubjectList();
    super.onInit();
  }

  void fetchSubjectList() async {
    try {
      isLoading(true);
      var summary = await SubjectListService.fetchSubjectList(token);
      subjectList = summary.data;
    } finally {
      isLoading(false);
    }
  }
}
